const Boom = require('@hapi/boom')

module.exports = (apiDebug, dbMan, aceNode) => {
  return fn =>
    async (request, h) => {
      const db = dbMan.get(request.params.dbname)
      if (db === null) {
        if (apiDebug) throw Boom.notFound(`DB ${request.params.dbname} not found`)
        throw Boom.notFound('DB not found')
      }
      const ace = aceNode.getDBInterface(db.address.toString())
      if (!ace) {
        if (apiDebug) throw Boom.notFound(`ACE Node for db ${request.params.dbname} not found`)
        throw Boom.notFound('ACE Node not found')
      }
      return Promise.resolve(fn(ace, request, h))
        .catch((err) => { throw err })
    }
}
