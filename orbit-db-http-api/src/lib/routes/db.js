const Boom = require('@hapi/boom')
const ACENode = require('eunomia-access-control').ACENode

const comparisons = {
  ne: (a, b) => a !== b,
  eq: (a, b) => a === b,
  gt: (a, b) => a > b,
  lt: (a, b) => a < b,
  gte: (a, b) => a >= b,
  lte: (a, b) => a <= b,
  mod: (a, b, c) => a % b === c,
  range: (a, b, c) => Math.max(b, c) >= a && a >= Math.min(b, c),
  all: () => true
}

const rawIterator = (db, request, _h) =>
  db.iterator(request.payload).collect()

const unpackContents = (contents) => {
  if (contents) {
    if (contents.map) {
      return contents.map((e) => {
        if (e.payload) return e.payload.value
        return e
      })
    } else if (contents.payload) {
      return contents.payload.value
    }
  }
  return contents
}

module.exports = function (managers, options, logger) {
  const dbMan = managers.dbManager
  const peerMan = managers.peerManager
  const dbMiddleware = require('../middleware/dbMiddleware.js')(options.orbitDBAPI.apiDebug, dbMan)

  const aceNode = new ACENode(managers.ipfs, options.aceNode)
  const aceMiddleware = require('../middleware/aceMiddleware.js')(options.orbitDBAPI.apiDebug, dbMan, aceNode)

  const addDBEventListener = (db, eventName, request, h) => {
    const eventMap = new Map(Object.entries({
      replicated: (address) =>
        h.event({ event: 'replicated', data: { address } }),
      replicate: (address) =>
        h.event({ event: 'replicate', data: { address } }),
      'replicate.progress': (address, hash, entry, progress, have) =>
        h.event({ event: 'replicate.progress', data: { address, hash, entry, progress, have } }),
      load: (address, heads) =>
        h.event({ event: 'load', data: { address, heads } }),
      'load.progress': (address, hash, entry, progress, total) =>
        h.event({ event: 'load.progress', data: { address, hash, entry, progress, total } }),
      ready: (address, heads) =>
        h.event({ event: 'ready', data: { address, heads } }),
      write: (address, hash, entry) =>
        h.event({ event: 'write', data: { address, hash, entry } }),
      closed: (address) =>
        h.event({ event: 'closed', data: { address } }),
      peer: (peer) =>
        h.event({ event: 'peer', data: { peer } }),
      'search.complete': (address, peers) => {
        h.event({ event: 'search.complete', data: { address, peers } })
      }
    }))

    const eventCallback = eventMap.get(eventName)

    if (eventCallback) {
      db.events.on(eventName, eventCallback)
      const keepAlive = setInterval(() => h.event({ event: 'keep-alive' }), 10000)
      request.events.on('disconnect', () => {
        db.events.removeListener(eventName, eventCallback)
        clearInterval(keepAlive)
      })
    } else {
      if (options.orbitDBAPI.apiDebug) throw Boom.badRequest(`Unrecognized event name: ${eventName}`)
      throw Boom.badRequest('Unrecognized event name')
    }
  }

  return [
    {
      method: ['POST', 'PUT'],
      path: '/db',
      handler: async (request, _h) => {
        const payload = request.payload
        let db
        db = dbMan.get(payload.dbname)
        if (db === null) {
          db = await dbMan.openCreate(payload.dbname, payload)
          await aceNode.addDB(db, payload.aceOptions)
        }
        if (!db) { // TODO: add docs
          return {}
        }
        return dbMan.dbInfo(db)
      }
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}',
      handler: async (request, _h) => {
        const payload = request.payload
        let db
        db = dbMan.get(request.params.dbname)
        if (db === null) {
          db = await dbMan.openCreate(request.params.dbname, payload)
          await aceNode.addDB(db, payload.aceOptions)
        }
        if (!db) { // TODO: add docs
          return {}
        }
        return dbMan.dbInfo(db)
      }
    },
    {
      method: 'GET',
      path: '/db/{dbname}',
      handler: dbMiddleware(async (db, _request, _h) => dbMan.dbInfo(db))
    },
    {
      method: 'GET',
      path: '/db/{dbname}/{item}',
      handler: aceMiddleware(async (ace, request, h) => {
        const raw = await ace.get(request.params.item)
        if (typeof raw === 'undefined') {
          if (options.orbitDBAPI.apiDebug) throw Boom.notFound(`Item ${request.params.item} not found`)
          throw Boom.notFound('Item not found')
        }
        const contents = unpackContents(raw) // TODO: we should probably remove this
        if (typeof contents === 'string') {
          return h.response(JSON.stringify(contents))
            .type('application/json')
        }
        return contents
      })
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/create',
      handler: aceMiddleware(async (ace, request, _h) => {
        const params = request.payload

        return { hash: await ace.create(params) }
      })
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/update',
      handler: aceMiddleware(async (ace, request, _h) => {
        const params = request.payload

        return { hash: await ace.update(params) }
      })
    },
    {
      method: 'DELETE',
      path: '/db/{dbname}/{item}',
      handler: aceMiddleware(async (ace, request, _h) => {
        if (ace.del) {
          return { hash: await ace.del(request.params.item) }
        } else {
          return Boom.methodNotAllowed(`This does not support removing data`,
            {
              dbname: ace.aceEntity.db.dbname,
              dbtype: ace.aceEntity.db.type
            })
        }
      })
    },
    { // get node's ID and public key
      method: 'GET',
      path: '/node-info',
      handler: async (request, _h) => {
        return {
          id: aceNode.id,
          publicKey: aceNode.publicKey
        }
      }
    },
    { // add an <id,publicKey> pair to the DB's state. Expects object { id: ..., publicKey: ... }
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/public-key',
      handler: aceMiddleware(async (ace, request, _h) => {
        const params = request.payload

        ace.addKey(params.id, params.publicKey)
        return {}
      })
    },
    { // trigger ACE node to update local store of public keys
      method: 'GET',
      path: '/update-public-keys',
      handler: async (request, _h) => {
        await aceNode.updatePublicKeys()
        return {}
      }
    },
    { // delete all retrieved object encryption keys
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/clear-keys',
      handler: aceMiddleware(async (ace, request, _h) => {
        ace.clearKeys()
        return {}
      })
    },
    { // Query method (over objects AFTER decryption) - TODO see if we should change this!!
      method: 'POST',
      path: '/db/{dbname}/query',
      handler: aceMiddleware(async (ace, request, _h) => {
        const qparams = request.payload
        logger.debug('Query request payload', qparams)

        if (typeof(qparams) == 'undefined') {
          let comparison = comparisons['all'];
          return ace.queryAll((doc) => comparison())
        }

        if (typeof(qparams.props) == 'undefined' || typeof(qparams.comps) == 'undefined' || typeof(qparams.values) == 'undefined') {
          let comparison = comparisons['all'];
          return ace.queryAll((doc) => comparison())
        }

        let props = qparams.props
        let comps = qparams.comps
        let values = qparams.values

        if (props == null || comps == null || values == null) {
          let comparison = comparisons['all'];
          return ace.queryAll((doc) => comparison())
        }

        if (props.length == 0 || comps.length == 0 || values.length == 0) {
          let comparison = comparisons['all'];
          return ace.queryAll((doc) => comparison())
        }

        let finalCondition = null;

        const query = (doc) => {
          for (let index = 0; index < comps.length; index++) {
            let comparison = comparisons[comps[index] || 'all']
            let propName = props[index].split('.')

            if (index == 0) {
              finalCondition = comparison(
                propName.reduce(function(object, key) {return object && object[key]}, doc),
                values[index]
              )
            } else {
              finalCondition = finalCondition && comparison(
                propName.reduce(function(object, key) {return object && object[key]}, doc),
                values[index]
              );
            }
          }
          return finalCondition;
        }

        return ace.queryAll(query)
      })
    },
    { // Cleartext Query method (over objects BEFORE decryption) - TODO see if we should change this!!
      method: 'POST',
      path: '/db/{dbname}/cleartext-query',
      handler: aceMiddleware(async (ace, request, _h) => {
        const qparams = request.payload
        logger.debug('Query request payload', qparams)

        if (typeof(qparams) == 'undefined') {
          let comparison = comparisons['all'];
          return ace.query((doc) => comparison())
        }

        if (typeof(qparams.props) == 'undefined' || typeof(qparams.comps) == 'undefined' || typeof(qparams.values) == 'undefined') {
          let comparison = comparisons['all'];
          return ace.query((doc) => comparison())
        }

        let props = qparams.props
        let comps = qparams.comps
        let values = qparams.values

        if (props == null || comps == null || values == null) {
          let comparison = comparisons['all'];
          return ace.query((doc) => comparison())
        }

        if (props.length == 0 || comps.length == 0 || values.length == 0) {
          let comparison = comparisons['all'];
          return ace.query((doc) => comparison())
        }

        let finalCondition = null;

        const query = (doc) => {
          for (let index = 0; index < comps.length; index++) {
            let comparison = comparisons[comps[index] || 'all']
            let propName = props[index].split('.')

            if (index == 0) {
              finalCondition = comparison(
                propName.reduce(function(object, key) {return object && object[key]}, doc),
                values[index]
              )
            } else {
              finalCondition = finalCondition && comparison(
                propName.reduce(function(object, key) {return object && object[key]}, doc),
                values[index]
              );
            }
          }
          return finalCondition;
        }

        return ace.query(query)
      })
    },
    {
      method: 'DELETE',
      path: '/db/{dbname}',
      handler: dbMiddleware(async (db, _request, _h) => {
        await peerMan.removeDB(db)
        await db.close()
        return {}
      })
    },


    // Things we are not using but keeping for compatibility
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/load',
      handler: dbMiddleware(async (db, request, _h) => {
        dbMan.loadDB(db)
        return {}
      })
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/sync',
      handler: dbMiddleware(async (db, request, _h) => {
        const params = request.payload
        dbMan.syncDB(db, params)
        return {}
      })
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/announce',
      handler: dbMiddleware(async (db, _request, _h) => {
        peerMan.announceDB(db)
        return {}
      })
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/add',
      handler: dbMiddleware(async (db, request, _h) => {
        return { hash: await db.add(request.payload) }
      })
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/inc',
      handler: dbMiddleware(async (db, request, _h) => {
        const incval = parseInt(request.payload && request.payload.val)
        return { hash: await db.inc(incval) }
      })
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/inc/{val}',
      handler: dbMiddleware(async (db, request, _h) => {
        return { hash: await db.inc(parseInt(request.params.val)) }
      })
    },
    {
      method: 'GET',
      path: '/db/{dbname}/iterator',
      handler: dbMiddleware(async (db, request, h) => {
        const raw = rawIterator(db, request, h)
        return raw.map((e) => Object.keys(e.payload.value)[0])
      })
    },
    {
      method: 'GET',
      path: '/db/{dbname}/oplog/heads',
      handler: dbMiddleware(async (db, request, h) => {
        const oplog = db.oplog || db._oplog
        return oplog.heads
      })
    },
    {
      method: 'GET',
      path: '/db/{dbname}/oplog/values',
      handler: dbMiddleware(async (db, request, h) => {
        const oplog = db.oplog || db._oplog
        return oplog.values
      })
    },
    {
      method: 'GET',
      path: '/db/{dbname}/rawiterator',
      handler: dbMiddleware(async (db, request, h) => {
        return rawIterator(db, request, h)
      })
    },
    {
      method: 'GET',
      path: '/db/{dbname}/raw/{item}',
      handler: dbMiddleware(async (db, request, h) => {
        const raw = await db.get(request.params.item)
        if (typeof raw === 'undefined') {
          if (options.orbitDBAPI.apiDebug) throw Boom.notFound(`Item ${request.params.item} not found`)
          throw Boom.notFound('Item not found')
        }
        if (typeof raw === 'string') {
          return h.response(JSON.stringify(raw))
            .type('application/json')
        }
        return raw
      })
    },
    {
      method: 'GET',
      path: '/db/{dbname}/all',
      handler: dbMiddleware(async (db, _request, _h) => {
        if (typeof db._query === 'function') {
          const contents = db._query({ limit: -1 })
          return contents.map((e) => Object.keys(e.payload.value)[0])
        } else {
          return unpackContents(db.all)
        }
      })
    },
    {
      method: 'GET',
      path: '/db/{dbname}/index',
      handler: dbMiddleware(async (db, _request, _h) => db.index)
    },
    {
      method: 'GET',
      path: '/db/{dbname}/value',
      handler: dbMiddleware(async (db, _request, _h) => db.value)
    },
    {
      method: ['POST', 'PUT'],
      path: '/db/{dbname}/access/write',
      handler: dbMiddleware(async (db, request, _h) => {
        const result = await db.access.grant('write', request.payload.id)
        if (result === false) {
          return Boom.notImplemented('Access controller does not support setting write access')
        }
        return result
      }
      )
    },
    {
      method: 'GET',
      path: '/db/{dbname}/access/write/list',
      handler: dbMiddleware(async (db, _request, _h) => dbMan.dbWrite(db))
    },
    {
      method: 'GET',
      path: '/db/{dbname}/events/{eventnames}',
      handler: dbMiddleware(async (db, request, h) => {
        const eventnames = request.params.eventnames
        const events = typeof eventnames === 'string' ? eventnames.split(',') : eventnames
        events.forEach((eventName) => addDBEventListener(db, eventName, request, h))
        return h.event({ event: 'registered', data: { events } })
      })
    },
    {
      method: 'GET',
      path: '/db/{dbname}/peers',
      handler: dbMiddleware((db, _request, _h) => peerMan.getPeers(db))
    }
  ]
}
