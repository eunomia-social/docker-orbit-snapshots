# Pull base image
FROM node:12

# Create base directory
RUN mkdir orbitdb

# Define working directory
WORKDIR /orbitdbsrc

# Copy package.json and package-lock.json
COPY . .

# Install dependencies
RUN bash docker-install.sh

# Expose port 3000
EXPOSE 3000

# Define working directory
WORKDIR /orbitdbsrc/orbit-db-http-api

# Command for starting app
CMD node --harmony src/cli.js api --ipfs-host ipfs --https-cert my.crt --https-key my.key
