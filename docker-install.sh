# docker-install.sh

echo "Installing orbit-db-store"
cd orbit-db-store
# git checkout develop
npm ci --only=prod

echo "Installing orbit-db-docstore"
cd ../orbit-db-docstore
# git checkout develop
npm ci --only=prod

echo "Installing orbit-db"
cd ../orbit-db
# git checkout develop
npm ci --only=prod

echo "Installing eunomia-access-control"
cd ../eunomia-access-control
# git checkout develop
npm ci --only=prod

echo "Installing orbit-db-http-api"
cd ../orbit-db-http-api
npm ci --only=prod

echo "Linking eunomia-access-control to orbit-db-http-api"
npm link ../eunomia-access-control
echo "Linking orbit-db to orbit-db-http-api"
npm link ../orbit-db

cd ../orbit-db
echo "Linking orbit-db-store to orbit-db"
npm link ../orbit-db-store
echo "Linking orbit-db-docstore to orbit-db"
npm link ../orbit-db-docstore

cd ../orbit-db-docstore
echo "Linking orbit-db-store to orbit-db-docstore"
npm link ../orbit-db-store
