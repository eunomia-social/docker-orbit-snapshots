const assert = require('assert')
const EunomiaNode = require('./utils/eunomia-node')
const sleep = require('./utils/sleep')
const wait4input = require('./utils/wait-for-input')
const { urlSS1, urlSS2, urlDB1, urlDB2 } = require('./utils/config')

const NUMBER_OF_UPDATES = 320

describe('Storage Server - initialization from snapshot', function () {
  this.timeout(0)

  let node1, node2, objID

  it('first storage server regular startup', async function () {
    node1 = new EunomiaNode(urlSS1, { dbUrl: urlDB1 })
    await node1.init()

    await sleep(500)

    const insertedObj = await node1.insertObject({'user': 'glou'})
    objID = insertedObj.data.object.id

    console.log(`----- invoking node1.updateObject ${NUMBER_OF_UPDATES} times`)
    for (let i = 0; i < NUMBER_OF_UPDATES; i++) {
      await node1.updateObject(objID, {'user': 'uolg', 'others': `hello${i}`})
    }

    // insert 5 more objects
    //  '--> this makes sure the last snapshot is not taken after
    //       a delete (part of the updateObject method)
    for (let i = 0; i < 5; i++) {
      await node1.insertObject({'user': `somthing${i}`})
    }

    // Wait for node 1 to finish taking snapshots
    await wait4input('Press enter when node 1 is ready...')

    let retrievedObj1 = await node1.getObject(objID)
    console.log(retrievedObj1)
    assert.deepStrictEqual(retrievedObj1.properties.user, 'uolg')
    assert.deepStrictEqual(retrievedObj1.properties.others, `hello${NUMBER_OF_UPDATES-1}`)

    console.log("----- node1.listObjects('user', 'ne', 'jo')")
    let listObjs = await node1.listObjects('user', 'ne', 'jo')
    console.log(listObjs.data.length)
    console.log(listObjs.data)
  })

  it('node 2 successfully joins - using snapshot', async function () {
    // Wait for node 2 to finish taking snapshots
    await wait4input('Start node 2 now and press enter when it is on...')

    node2 = new EunomiaNode(urlSS2, { dbUrl: urlDB2 })
    await node2.init()

    // Wait for node 2 to finish taking snapshots
    await wait4input('Press enter when node 2 is ready...')

    let retrievedObj2 = await node2.getObject(objID)
    console.log(retrievedObj2)
    assert.deepStrictEqual(retrievedObj2.properties.user, 'uolg')
    assert.deepStrictEqual(retrievedObj2.properties.others, `hello${NUMBER_OF_UPDATES-1}`)

    const oplog1 = await node1.getOpLog()
    console.log(`oplog1: ${oplog1.length}`)
    const oplog2 = await node2.getOpLog()
    console.log(`oplog2: ${oplog2.length}`)
    assert(oplog2.length < oplog1.length)
    assert(oplog2.length < 20)

    const lcoplog1 = await node1.getOpLog('ledgercache')
    console.log(`lcoplog1: ${lcoplog1.length}`)
    const lcoplog2 = await node2.getOpLog('ledgercache')
    console.log(`lcoplog2: ${lcoplog2.length}`)
    assert(lcoplog2.length < lcoplog1.length)
    assert(lcoplog2.length < 20)
  })

  it('insert objects on node 1', async function () {
    // Get the operation logs before inserting objects
    const oplog1 = await node1.getOpLog()
    const oplog2 = await node2.getOpLog()

    // Insert objects
    const insertedObj1 = await node1.insertObject({'user': 'new user'})
    const insertedObj2 = await node1.insertObject({'user': 'old person'})
    const insertedObj3 = await node1.insertObject({'user': 'thinking'})

    // Wait for replication to finish
    await wait4input('Press enter when nodes are ready...')

    // Confirm both nodes have the same view of the db state
    let objDB1 = await node1.getObject(insertedObj1.data.object.id)
    let objDB2 = await node2.getObject(insertedObj1.data.object.id)
    assert.deepStrictEqual(objDB1.properties.user, 'new user')
    assert.deepStrictEqual(objDB1, objDB2)
    objDB1 = await node1.getObject(insertedObj2.data.object.id)
    objDB2 = await node2.getObject(insertedObj2.data.object.id)
    assert.deepStrictEqual(objDB1.properties.user, 'old person')
    assert.deepStrictEqual(objDB1, objDB2)
    objDB1 = await node1.getObject(insertedObj3.data.object.id)
    objDB2 = await node2.getObject(insertedObj3.data.object.id)
    assert.deepStrictEqual(objDB1.properties.user, 'thinking')
    assert.deepStrictEqual(objDB1, objDB2)

    // Check that the size of the logs increased correctly
    const insertNumber = 3
    const newoplog1 = await node1.getOpLog()
    assert.equal(oplog1.length + insertNumber, newoplog1.length)
    const newoplog2 = await node2.getOpLog()
    assert.equal(oplog2.length + insertNumber, newoplog2.length)
  })

  it('insert objects on node 2', async function () {
    // Get the oplogs before inserting objects
    const oplog1 = await node1.getOpLog()
    const oplog2 = await node2.getOpLog()

    // Insert objects
    const insertedObj1 = await node2.insertObject({'user': 'up'})
    const insertedObj2 = await node2.insertObject({'user': 'down'})
    const insertedObj3 = await node2.insertObject({'user': 'left'})
    const insertedObj4 = await node2.insertObject({'user': 'right'})

    // Wait for replication to finish
    await wait4input('Press enter when nodes are ready...')

    // Confirm both nodes have the same view of the db state
    let objDB1 = await node1.getObject(insertedObj1.data.object.id)
    let objDB2 = await node2.getObject(insertedObj1.data.object.id)
    assert.deepStrictEqual(objDB1.properties.user, 'up')
    assert.deepStrictEqual(objDB1, objDB2)
    objDB1 = await node1.getObject(insertedObj2.data.object.id)
    objDB2 = await node2.getObject(insertedObj2.data.object.id)
    assert.deepStrictEqual(objDB1.properties.user, 'down')
    assert.deepStrictEqual(objDB1, objDB2)
    objDB1 = await node1.getObject(insertedObj3.data.object.id)
    objDB2 = await node2.getObject(insertedObj3.data.object.id)
    assert.deepStrictEqual(objDB1.properties.user, 'left')
    assert.deepStrictEqual(objDB1, objDB2)
    objDB1 = await node1.getObject(insertedObj4.data.object.id)
    objDB2 = await node2.getObject(insertedObj4.data.object.id)
    assert.deepStrictEqual(objDB1.properties.user, 'right')
    assert.deepStrictEqual(objDB1, objDB2)

    // Check that the size of the logs increased correctly
    const insertNumber = 4
    const newoplog1 = await node1.getOpLog()
    assert.equal(oplog1.length + insertNumber, newoplog1.length)
    const newoplog2 = await node2.getOpLog()
    assert.equal(oplog2.length + insertNumber, newoplog2.length)
  })
})
