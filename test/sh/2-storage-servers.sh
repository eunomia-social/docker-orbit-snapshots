#!/bin/bash

url="http://localhost:"
port1="3000"
port2="3001"

node1=${url}${port1}
node2=${url}${port2}

dbname=model


echo "---- see node1 log"
curl -X GET ${node1}/db/${dbname}/oplog/values; echo

echo "---- see node2 log"
curl -X GET ${node2}/db/${dbname}/oplog/values; echo

