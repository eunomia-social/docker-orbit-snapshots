echo '######################## QUERY TEST ########################'

echo '------------ Create DB called testdb ------------'
curl -X POST http://localhost:3000/db/testdb -d "create=true" -d "type=docstore"; echo

sleep 0.5
echo
echo '------------ Get info on testdb ------------'
curl -X GET http://localhost:3000/db/testdb; echo

sleep 0.5
echo
echo '------------ Add {"_id":2, "value": "testing", "likes": 400} to DB ------------'
curl -X POST http://localhost:3000/db/testdb/put -H "Content-Type: application/json" -d '{"_id":2, "value": "testing", "likes": 400}'; echo

sleep 0.5
echo
echo '------------ Add {"_id":3, "value": "threeeee", "likes": 200} to DB ------------'
curl -X POST http://localhost:3000/db/testdb/put -H "Content-Type: application/json" -d '{"_id":3, "value": "threeeee", "likes": 200}'; echo

sleep 0.5
echo
echo '------------ Add {"_id":4, "value": "yaaaa", "likes": 450} to DB ------------'
curl -X POST http://localhost:3000/db/testdb/put -H "Content-Type: application/json" -d '{"_id":4, "value": "yaaaa", "likes": 450}'; echo


sleep 0.5
echo
echo '------------ Query for likes > 300 ------------'
curl -X POST http://localhost:3000/db/testdb/query -H "Content-Type: application/json" -d '{"propname":"likes","comp":"gt","values":[300]}'; echo

sleep 0.5
echo
echo '------------ Query for likes > 150 ------------'
curl -X POST http://localhost:3000/db/testdb/query -H "Content-Type: application/json" -d '{"propname":"likes","comp":"gt","values":[150]}'; echo

sleep 0.5
echo
echo '------------ Query for range [300, 420] ------------'
curl -X POST http://localhost:3000/db/testdb/query -H "Content-Type: application/json" -d '{"propname":"likes","comp":"range","values":[300, 420]}'; echo

