#!/bin/bash

url="http://localhost:"
port1="3000"
port2="3001"

node1=${url}${port1}
node2=${url}${port2}

dbname=$1

echo "---- Create db ${dbname} on node1 with snapshotHeuristics:LogSize:5"
curl -X POST ${node1}/db/${dbname} -H "Content-Type: application/json" -d '{"create": "true", "type": "docstore", "accessController": {"write": ["*"]}, "snapshotHeuristics": {"type":"LogSize", "limit":5}}'; echo

# echo "---- introduce 7 items in the DB (node1)"
# curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 10}'; echo
# curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":2, "value": "testing2", "likes": 20}'; echo
# curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":3, "value": "testing3", "likes": 30}'; echo
# curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":4, "value": "testing4", "likes": 40}'; echo
# curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":5, "value": "testing5", "likes": 50}'; echo
# curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":6, "value": "testing6", "likes": 60}'; echo
# curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":7, "value": "testing7", "likes": 70}'; echo

echo "---- introduce 1 item and update 6 times (on node1)"
curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 10}'; echo
curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 20}'; echo
curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 30}'; echo
curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 40}'; echo
curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 50}'; echo
curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 60}'; echo
curl -X POST ${node1}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 70}'; echo

echo "---- get state of object 1 (on node1)"
curl -X GET ${node1}/db/${dbname}/1; echo

echo "---- now execute pt-2-curl-snap-test.sh passing the db address as argument"
