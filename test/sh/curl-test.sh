# curl-test.sh

echo '++++++++++++ Test a regular DB ++++++++++++'; echo

echo '------------ Get OrbitDB node info + available DBs ------------'
curl -X GET http://localhost:3000/dbs; echo

sleep 0.5
echo
echo '------------ Create DB called testdb ------------'
curl -X POST http://localhost:3000/db/testdb -d "create=true" -d "type=docstore"; echo

sleep 0.5
echo
echo '------------ Get info on testdb ------------'
curl -X GET http://localhost:3000/db/testdb; echo

sleep 0.5
echo
echo '------------ Add {"_id":2, "value": "testing", "details": "cool"} to DB ------------'
curl -X POST http://localhost:3000/db/testdb/put -H "Content-Type: application/json" -d '{"_id":2, "value": "testing", "details": "cool"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 2 from DB testdb ------------'
curl -X GET http://localhost:3000/db/testdb/2; echo

sleep 0.5
echo
echo '------------ Update {"_id":2, "value": "testinggnitset", "details": "coollooc"} to DB ------------'
curl -X POST http://localhost:3000/db/testdb/put -H "Content-Type: application/json" -d '{"_id":2, "value": "testinggnitset", "details": "coollooc"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 2 from DB testdb ------------'
curl -X GET http://localhost:3000/db/testdb/2; echo

sleep 0.5
echo
echo '------------ Add {"_id":3, "value": "third", "details": "mole"} to DB ------------'
curl -X POST http://localhost:3000/db/testdb/put -H "Content-Type: application/json" -d '{"_id":3, "value": "third", "details": "mole"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 3 from DB testdb ------------'
curl -X GET http://localhost:3000/db/testdb/3; echo

sleep 0.5
echo
echo '------------ Get non-existent entry from DB testdb ------------'
curl -X GET http://localhost:3000/db/testdb/4; echo

sleep 0.5
echo
echo '------------ Delete entry with id 3 from DB testdb ------------'
curl -X DELETE http://localhost:3000/db/testdb/3; echo

sleep 0.5
echo
echo '------------ Try to get entry with id 3 from DB testdb ------------'
curl -X GET http://localhost:3000/db/testdb/3; echo



# ++++++++++++ Test DB with snapshotHeuristics ++++++++++++
echo; echo; echo; echo '++++++++++++ Test DB with snapshotHeuristics ++++++++++++'; echo
################### LogSize Heuristic ###################
echo '################### LogSize Heuristic ###################'; echo

sleep 0.5
echo '------------ Get OrbitDB node info + available DBs ------------'
curl -X GET http://localhost:3000/dbs; echo

sleep 0.5
echo
echo '------------ Create DB called testdbheur ------------'
curl -X POST http://localhost:3000/db/testdbheur -H "Content-Type: application/json" --data '{"create": "true", "type": "docstore", "accessController": {"write": ["*"]}, "snapshotHeuristics": {"type":"LogSize", "limit":5}}'; echo

sleep 0.5
echo
echo '------------ Get info on testdbheur ------------'
curl -X GET http://localhost:3000/db/testdbheur; echo

sleep 0.5
echo
echo '------------ Add {"_id":2, "value": "testing", "details": "cool"} to DB ------------'
curl -X POST http://localhost:3000/db/testdbheur/put -H "Content-Type: application/json" -d '{"_id":2, "value": "testing", "details": "cool"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 2 from DB testdbheur ------------'
curl -X GET http://localhost:3000/db/testdbheur/2; echo

sleep 0.5
echo
echo '------------ Update {"_id":2, "value": "testinggnitset", "details": "coollooc"} to DB ------------'
curl -X POST http://localhost:3000/db/testdbheur/put -H "Content-Type: application/json" -d '{"_id":2, "value": "testinggnitset", "details": "coollooc"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 2 from DB testdbheur ------------'
curl -X GET http://localhost:3000/db/testdbheur/2; echo

sleep 0.5
echo
echo '------------ Add {"_id":3, "value": "third", "details": "mole"} to DB ------------'
curl -X POST http://localhost:3000/db/testdbheur/put -H "Content-Type: application/json" -d '{"_id":3, "value": "third", "details": "mole"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 3 from DB testdbheur ------------'
curl -X GET http://localhost:3000/db/testdbheur/3; echo

sleep 0.5
echo
echo '------------ Get non-existent entry from DB testdbheur ------------'
curl -X GET http://localhost:3000/db/testdbheur/4; echo

sleep 0.5
echo
echo '------------ Delete entry with id 3 from DB testdbheur ------------'
curl -X DELETE http://localhost:3000/db/testdbheur/3; echo

sleep 0.5
echo
echo '------------ Try to get entry with id 3 from DB testdbheur ------------'
curl -X GET http://localhost:3000/db/testdbheur/3; echo



################### TimeInterval Heuristic ###################
echo; echo; echo '################### TimeInterval Heuristic ###################'; echo

sleep 0.5
echo '------------ Get OrbitDB node info + available DBs ------------'
curl -X GET http://localhost:3000/dbs; echo

sleep 0.5
echo
echo '------------ Create DB called testdbtimeheur ------------'
curl -X POST http://localhost:3000/db/testdbtimeheur -H "Content-Type: application/json" --data '{"create": "true", "type": "docstore", "accessController": {"write": ["*"]}, "snapshotHeuristics": {"type":"TimeInterval", "period":1000}}'; echo

sleep 0.5
echo
echo '------------ Get info on testdbtimeheur ------------'
curl -X GET http://localhost:3000/db/testdbtimeheur; echo

sleep 0.5
echo
echo '------------ Add {"_id":2, "value": "testing", "details": "cool"} to DB ------------'
curl -X POST http://localhost:3000/db/testdbtimeheur/put -H "Content-Type: application/json" -d '{"_id":2, "value": "testing", "details": "cool"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 2 from DB testdbtimeheur ------------'
curl -X GET http://localhost:3000/db/testdbtimeheur/2; echo

sleep 0.5
echo
echo '------------ Update {"_id":2, "value": "testinggnitset", "details": "coollooc"} to DB ------------'
curl -X POST http://localhost:3000/db/testdbtimeheur/put -H "Content-Type: application/json" -d '{"_id":2, "value": "testinggnitset", "details": "coollooc"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 2 from DB testdbtimeheur ------------'
curl -X GET http://localhost:3000/db/testdbtimeheur/2; echo

sleep 0.5
echo
echo '------------ Add {"_id":3, "value": "third", "details": "mole"} to DB ------------'
curl -X POST http://localhost:3000/db/testdbtimeheur/put -H "Content-Type: application/json" -d '{"_id":3, "value": "third", "details": "mole"}'; echo

sleep 0.5
echo
echo '------------ Get entry with id 3 from DB testdbtimeheur ------------'
curl -X GET http://localhost:3000/db/testdbtimeheur/3; echo

sleep 0.5
echo
echo '------------ Get non-existent entry from DB testdbtimeheur ------------'
curl -X GET http://localhost:3000/db/testdbtimeheur/4; echo

sleep 0.5
echo
echo '------------ Delete entry with id 3 from DB testdbtimeheur ------------'
curl -X DELETE http://localhost:3000/db/testdbtimeheur/3; echo

sleep 0.5
echo
echo '------------ Try to get entry with id 3 from DB testdbtimeheur ------------'
curl -X GET http://localhost:3000/db/testdbtimeheur/3; echo


sleep 0.5
echo
echo '------------ Get OrbitDB node info + available DBs ------------'
curl -X GET http://localhost:3000/dbs; echo

echo; echo '-+-+-+-+-+-+-+-+-+-+-+- Tests finished -+-+-+-+-+-+-+-+-+-+-+-'; echo
