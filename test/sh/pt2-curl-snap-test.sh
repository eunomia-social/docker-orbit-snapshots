#!/bin/bash

url="http://localhost:"
port1="3000"
port2="3001"

node1=${url}${port1}
node2=${url}${port2}

dbname=$1
dbaddr=$2
dbfullname=$dbaddr'%2F'${dbname}

# options='{"name":"'
# options+="${dbaddr}"
# options+='", "type":"docstore", "indexSnapshot":"true"}'

options='{"sync": true, "indexSnapshot": true}'

echo options: $options

echo "---- node2 joins the network"
# curl -X POST ${node2}/db/${dbfullname}; echo
curl -X POST ${node2}/db/${dbfullname} -H "Content-Type: application/json" -d "$options"; echo

echo "---- sleep for 3s seconds"
sleep 3s

# echo "---- get object 1"
# curl -X GET ${node2}/db/${dbname}/1; echo
# echo "---- get object 2"
# curl -X GET ${node2}/db/${dbname}/2; echo
# echo "---- get object 3"
# curl -X GET ${node2}/db/${dbname}/3; echo
# echo "---- get object 4"
# curl -X GET ${node2}/db/${dbname}/4; echo
# echo "---- get object 5"
# curl -X GET ${node2}/db/${dbname}/5; echo
# echo "---- get object 6"
# curl -X GET ${node2}/db/${dbname}/6; echo
# echo "---- get object 7"
# curl -X GET ${node2}/db/${dbname}/7; echo

echo "---- get object 1"
curl -X GET ${node2}/db/${dbname}/1; echo

echo "---- see node1 log"
curl -X GET ${node1}/db/${dbname}/oplog/values; echo

echo "---- see node2 log"
curl -X GET ${node2}/db/${dbname}/oplog/values; echo

# Usar eventualmente!!!!!

# method: 'GET',
# path: '/db/{dbname}/oplog/values',
# curl -X GET ${node1}/db/${dbname}/oplog/values; echo
