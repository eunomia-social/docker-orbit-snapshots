module.exports = async msg => await new Promise(resolve => {
  const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
  });

  readline.question(msg, (txt) => {
    readline.close();
    resolve(txt);
  });
})
