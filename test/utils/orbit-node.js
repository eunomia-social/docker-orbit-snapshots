const axios = require('axios')
const Database = require('./database')

class OrbitNode {
  constructor(nodeUrl) {
    this.nodeUrl = nodeUrl
    this.dbs = {}
  }

  async getDBs () {
    // curl -X GET http://{nodeUrl}/dbs
    const res = await axios.get(this.nodeUrl + '/dbs')
    return res.data
  }

  async createDB (dbName, options = {}) {
    // curl -X POST {nodeUrl}/db/{name} -H "Content-Type: application/json" --data '{"create": "true", "type": "docstore", "accessController": {"write": ["*"]}}'
    const defaultOptions = {
      "create": "true",
      "type": "docstore",
      "accessController": {"write": ["*"]}
    }

    const opts = Object.assign({}, defaultOptions, options)

    const db = await Database.open(this.nodeUrl, dbName, opts)
    this.dbs[dbName] = db
    return db
  }

  async openDB (address, options = {}) {
    const defaultOptions = {
      "type": "docstore",
      "accessController": {"write": ["*"]}
    }

    const opts = Object.assign({}, defaultOptions, options)

    const fullName = `${address.root}%2F${address.path}`
    const db = await Database.open(this.nodeUrl, fullName, opts)

    this.dbs[address.path] = db
    return db
  }

  async deleteDB (db) {
    await db.deleteDB()
    delete this.dbs[db.name]
  }

  async updatePublicKeys () {
    const res = await axios.get(this.nodeUrl + '/update-public-keys')
    return res.data
  }
}

module.exports = OrbitNode
