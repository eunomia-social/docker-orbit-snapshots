const axios = require('axios')

const MASTODON_TOKEN = 'Yr0s7gYKLQvxof17VBsBlrNMNC_se8d4AOz4APCfKNI'
const AUTH_PROVIDER_URI = 'mastodon.social'
const TOKEN_ADDR = (url) => url + '/api/eunomia/token/'
const STORAGE_ADDR = (url) => url + '/api/objects/'
const DB_ADDR = (url) => url + '/'
const CLEARTEXT_QUERY = (url) => url + '/api/objects-cleartext'

class EunomiaNode {
  constructor(url, { dbUrl, token } = {}) {
    this.url = url
    this.storageAddr = STORAGE_ADDR(this.url)

    if (dbUrl) {
      this.dbUrl = dbUrl
      this.dbAddr = DB_ADDR(this.dbUrl)
    }

    if (token) {
      this.token = token
      this.tokenParams = { params: { access_token: this.token } }
    }
  }

  async init() {
    if (!this.token) {
      this.token = await this._getEunomiaToken()
      this.tokenParams = { params: { access_token: this.token } }
    }
    console.log(`EUNOMIA token: ${this.token}`)
  }

  async _getEunomiaToken (token=MASTODON_TOKEN, auth_provider_uri=AUTH_PROVIDER_URI) {
    const params = {
      access_token: token,
      auth_provider_uri
    }
    const res = await axios.get(TOKEN_ADDR(this.url), { params })
    return res.data.data.token
  }

  async listObjects (property, comp, value) {
    const res = await axios.get(this.storageAddr, {
      params: {
        property,
        comp,
        value,
        access_token: this.token
      }
    })
    return res.data
  }

  async listObjectsCleartext (property, comp, value) {
    const res = await axios.get(CLEARTEXT_QUERY(this.url), {
      params: {
        property,
        comp,
        value,
        access_token: this.token
      }
    })
    return res.data
  }

  async getObjectResponse (id) {
    const res = await axios.get(this.storageAddr + id, this.tokenParams)
    return res.data
  }
  async getObject (id) {
    return (await this.getObjectResponse(id)).data.object
  }

  async insertObjectNoTemplate (obj) {
    const res = await axios.post(this.storageAddr, obj, this.tokenParams)
    return res.data
  }
  async insertObject (obj) {
    return await this.insertObjectNoTemplate(this._objectTemplate(obj))
  }

  async updateObjectNoTemplate (id, obj) {
    const res = await axios.put(this.storageAddr + id, obj, this.tokenParams)
    return res.data
  }
  async updateObject (id, obj) {
    return await this.updateObjectNoTemplate(id, this._objectTemplate(obj))
  }

  async deleteObject (id) {
    const res = await axios.delete(this.storageAddr + id, this.tokenParams)
    return res.data
  }

  _objectTemplate (props) {
    return {
      properties: props || {},
      type: "string",
      validated: true
    }
  }

  async getOpLog(db = 'model') {
    const res = await axios.get(this.dbAddr + 'db/' + db + '/oplog/values')
    return res.data
  }

  async getNodeInfo () {
    const res = await axios.get(this.dbAddr + 'node-info')
    return res.data
  }

  async getNodeID () {
    const data = await this.getNodeInfo()
    return data.id
  }
}

module.exports = EunomiaNode
