const axios = require('axios')

class Database {
  constructor (nodeUrl, address, options) {
    this.nodeUrl = nodeUrl
    this.address = address
    this.name = address.path
    this.options = options

    this.dbUrl = nodeUrl + '/db/' + address.path
  }

  // curl -X POST ${nodeUrl}/db/${dbname}/put -H "Content-Type: application/json" -d '{"_id":1, "value": "testing1", "likes": 10}'
  async put (obj) {
    const res = await axios.post(this.dbUrl + '/put', obj)
    return res.data
  }

  async create (obj) {
    const res = await axios.post(this.dbUrl + '/create', obj)
    return res.data
  }

  async update (obj) {
    const res = await axios.post(this.dbUrl + '/update', obj)
    return res.data
  }

  // curl -X GET ${nodeUrl}/db/${dbname}/{id}
  async get (id) {
    const res = await axios.get(this.dbUrl + '/' + id)
    return res.data
  }

  async delete (id) {
    const res = await axios.delete(this.dbUrl + '/' + id)
    return res.data
  }

  async getAceData () {
    const res = await axios.get(this.dbUrl + '/ace-data')
    return res.data
  }

  async addPublicKey (obj) {
    const res = await axios.post(this.dbUrl + '/public-key', obj)
    return res.data
  }

  async updatePublicKeys () {
    const res = await axios.get(this.nodeUrl + '/update-public-keys')
    // const res = await axios.get(this.dbUrl + '/update-public-keys')
    return res.data
  }

  async getOpLog () {
    const res = await axios.get(this.dbUrl + '/oplog/values')
    return res.data
  }

  async deleteDB () {
    const res = await axios.delete(this.dbUrl)
    return res.data
  }

  static async open (nodeUrl, name, options = {}) {
    const res = await axios.post(nodeUrl + '/db/' + name, options)

    return new Database(nodeUrl, res.data.address, options)
  }
}

module.exports = Database