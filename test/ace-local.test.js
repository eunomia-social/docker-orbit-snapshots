'use strict'

const assert = require('assert')
const OrbitNode = require('./utils/orbit-node')
const sleep = require('./utils/sleep')
const wait4input = require('./utils/wait-for-input')

const urlDB1 = 'http://localhost:3000'
const urlDB2 = 'http://localhost:3001'
const urlDB3 = 'http://localhost:3002'

describe(`OrbitDB - initialization from snapshot`, function () {
  this.timeout(0);

  let o1, o2, o3, dbname

  before(async function () {
    o1 = new OrbitNode(urlDB1)
    o2 = new OrbitNode(urlDB2)
    o3 = new OrbitNode(urlDB3)

    let dbs1 = await o1.getDBs()
    let dbs2 = await o2.getDBs()
    let dbs3 = await o2.getDBs()
    // assert.equal(dbs1.length, 0)
    // assert.equal(dbs2.length, 0)
    // assert.equal(dbs3.length, 0)

    dbname = Math.random().toString(16).substring(7)
  })

  after(async function () {
    Object.values(o1.dbs).forEach(async db => await o1.deleteDB(db))
    Object.values(o2.dbs).forEach(async db => await o2.deleteDB(db))
    Object.values(o3.dbs).forEach(async db => await o3.deleteDB(db))
    await sleep(2000)
  })

  describe('One database, 3 nodes', function () {
    let options, db1, db2, db3

    before(async function () {
      console.log(`dbname: ${dbname}`)

      db1 = await o1.createDB(dbname)
      let dbs1 = await o1.getDBs()
      // assert.equal(dbs1.length, 1)

      let dbs2 = await o2.getDBs()
      // assert.equal(dbs2.length, 0)

      let dbs3 = await o3.getDBs()
      // assert.equal(dbs3.length, 0)

      let opLog1 = await db1.getOpLog()
      assert.equal(opLog1.length, 0)

      // await wait4input('Press enter to open db on nodes 2...')
      await wait4input('Press enter to open db on nodes 2 and 3...')
      db2 = await o2.openDB(db1.address, options)

      // await wait4input('Press enter to open db on node 3...')
      db3 = await o3.openDB(db1.address, options)

      await wait4input('Press enter when all nodes are ready...')
      dbs1 = await o1.getDBs()
      dbs2 = await o2.getDBs()
      dbs3 = await o3.getDBs()
      // assert.equal(dbs1.length, 1)
      // assert.equal(dbs2.length, 1)
      // assert.equal(dbs3.length, 1)
    })

    let pk1, pk2, pk3

    it('updates nodes public keys', async function () {
      await db1.updatePublicKeys()
      await db2.updatePublicKeys()
      await db3.updatePublicKeys()
    })

    it('create object on node 1 and correctly read it from all nodes', async function () {
      const obj = {
        _id: 1,
        value: "testing node 1",
        details: "cool",
        permissions: { write: [], delete: [] }
      }

      await db1.create(obj)

      await wait4input('Press enter when all nodes have replicated object...')

      let retrieved1 = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved1.length, 1)
      assert.deepStrictEqual(retrieved1[0], obj)

      let retrieved2 = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved2.length, 1)
      assert.deepStrictEqual(retrieved2[0], obj)

      let retrieved3 = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved3.length, 1)
      assert.deepStrictEqual(retrieved3[0], obj)

      retrieved1 = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved1.length, 1)
      assert.deepStrictEqual(retrieved1[0], obj)
      retrieved2 = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved2.length, 1)
      assert.deepStrictEqual(retrieved2[0], obj)
      retrieved3 = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved3.length, 1)
      assert.deepStrictEqual(retrieved3[0], obj)
    })

    it('create object on node 2 and correctly read it from all nodes', async function () {
      const obj = {
        _id: 2,
        value: "testing node 2",
        details: "quite cooler",
        permissions: { write: [], delete: [] }
      }

      await db2.create(obj)

      await wait4input('Press enter when all nodes have replicated object...')

      let retrieved1 = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved1.length, 1)
      assert.deepStrictEqual(retrieved1[0], obj)

      let retrieved2 = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved2.length, 1)
      assert.deepStrictEqual(retrieved2[0], obj)

      let retrieved3 = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved3.length, 1)
      assert.deepStrictEqual(retrieved3[0], obj)

      retrieved1 = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved1.length, 1)
      assert.deepStrictEqual(retrieved1[0], obj)
      retrieved2 = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved2.length, 1)
      assert.deepStrictEqual(retrieved2[0], obj)
      retrieved3 = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved3.length, 1)
      assert.deepStrictEqual(retrieved3[0], obj)
    })

    it('create object on node 3 and correctly read it from all nodes', async function () {
      const obj = {
        _id: 3,
        value: "testing node 3",
        details: "a lot cooler",
        permissions: { write: [], delete: [] }
      }

      await db3.create(obj)

      await wait4input('Press enter when all nodes have replicated object...')

      let retrieved1 = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved1.length, 1)
      assert.deepStrictEqual(retrieved1[0], obj)

      let retrieved2 = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved2.length, 1)
      assert.deepStrictEqual(retrieved2[0], obj)

      let retrieved3 = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved3.length, 1)
      assert.deepStrictEqual(retrieved3[0], obj)

      retrieved1 = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved1.length, 1)
      assert.deepStrictEqual(retrieved1[0], obj)
      retrieved2 = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved2.length, 1)
      assert.deepStrictEqual(retrieved2[0], obj)
      retrieved3 = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved3.length, 1)
      assert.deepStrictEqual(retrieved3[0], obj)
    })

    const compareObjects = (original, retrieved) => {
      if (typeof(original) === 'object') {
        return Object.keys(original).reduce((acc, key) => acc && compareObjects(original[key], retrieved[key]), true)
      } else {
        return original === retrieved
      }
    }

    it('create non encrypted object on node 1 and correctly read it from all nodes', async function () {
      const obj = {
        _id: 11,
        value: "testing node 1",
        details: "cool",
        permissions: { write: [], delete: [] },
        nonEncrypted: true
      }

      await db1.create(obj)

      await wait4input('Press enter when all nodes have replicated object...')

      let retrieved = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
      retrieved = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
      retrieved = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
    })

    it('create non encrypted object on node 2 and correctly read it from all nodes', async function () {
      const obj = {
        _id: 22,
        value: "testing node 2",
        details: "quite cooler",
        permissions: { write: [], delete: [] },
        nonEncrypted: true
      }

      await db2.create(obj)

      await wait4input('Press enter when all nodes have replicated object...')

      let retrieved = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
      retrieved = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
      retrieved = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
    })

    it('create non encrypted object on node 3 and correctly read it from all nodes', async function () {
      const obj = {
        _id: 33,
        value: "testing node 3",
        details: "a lot cooler",
        permissions: { write: [], delete: [] },
        nonEncrypted: true
      }

      await db3.create(obj)

      await wait4input('Press enter when all nodes have replicated object...')

      let retrieved = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
      retrieved = await db2.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
      retrieved = await db3.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
    })
  })
})

