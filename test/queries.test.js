const assert = require('assert')
const EunomiaNode = require('./utils/eunomia-node')
const Database = require('./utils/database')
const sleep = require('./utils/sleep')

const sleepAndLog = async (delay, msg) => {
  // console.log(msg)
  await sleep(delay)
}

const urlDB1 = 'http://localhost:3000'
const urlDB2 = 'http://localhost:3001'
const urlDB3 = 'http://localhost:3002'

const urlSS1 = 'http://0.0.0.0:5000'
const urlSS2 = 'http://0.0.0.0:5002'
const urlSS3 = 'http://0.0.0.0:5003'

const compareObjects = (original, retrieved) => {
  if (typeof(original) === 'object') {
    return Object.keys(original).reduce((acc, key) => acc && compareObjects(original[key], retrieved[key]), true)
  } else {
    return original === retrieved
  }
}

describe('Storage Server - testing queries', function () {
  this.timeout(0)

  let node1, node2, node3

  before(async function () {
    node1 = new EunomiaNode(urlSS1, { dbUrl: urlDB1 })
    await node1.init()
    node2 = new EunomiaNode(urlSS2, { dbUrl: urlDB2, token: node1.token })
    await node2.init()
    node3 = new EunomiaNode(urlSS3, { dbUrl: urlDB3, token: node1.token })
    await node3.init()

    const address = (dbName) => ({ path: dbName })
    node1.db = new Database(urlDB1, address('model'))
    node2.db = new Database(urlDB2, address('model'))
    node3.db = new Database(urlDB3, address('model'))

    node1.id = await node1.getNodeID()
    console.log(`NODE 1 ID: ${node1.id}`)
    node2.id = await node2.getNodeID()
    console.log(`NODE 2 ID: ${node2.id}`)
    node3.id = await node3.getNodeID()
    console.log(`NODE 3 ID: ${node3.id}`)
  })

  it('update public keys', async function () {
    await sleepAndLog(500, 'trigger public key retrieval from blockchain...')
    await node1.db.updatePublicKeys()
    await node2.db.updatePublicKeys()
    await node3.db.updatePublicKeys()
  })

  // TODO change all occurances of `cleartextProperties` to `cleartextProperties`
  describe('using cleartextProperties', async function () {
    let obj1, obj2, obj3

    it('insert 2 encrypted objects in node 1', async function () {
      await sleepAndLog(500, 'insert object in node 1...')

      obj1 = {
        properties: {
          prop1: 'propertyInCleartext',
          prop2: 'qqwwe'
        },
        cleartextProperties: {
          prop1: 'propertyInCleartext'
        },
        type: 'oNeTyPe',
        permissions: { read: ['*'] },
      }
      await node1.insertObjectNoTemplate(obj1)

      obj2 = {
        properties: {
          prop1: 'propertyInCleartext',
          prop2: '123456789'
        },
        cleartextProperties: {
          prop1: 'propertyInCleartext'
        },
        type: 'aNoThErTyPe',
        permissions: { read: ['*'] },
      }
      await node1.insertObjectNoTemplate(obj2)

      obj3 = {
        properties: {
          prop1: 'this-one',
          prop2: 'should-not-be-found'
        },
        cleartextProperties: {
          prop1: 'this-one',
        },
        type: 'iNvIsIbLeTyPe',
        permissions: { read: ['*'] },
      }
      await node1.insertObjectNoTemplate(obj3)

      await sleepAndLog(500, 'start querying...')
    })

    it('query nodes using regular query: properties.prop1 === "propertyInCleartext"', async function () {
      const listObjs1 = await node1.listObjects('properties.prop1', 'eq', 'propertyInCleartext')
      const listObjs2 = await node2.listObjects('properties.prop1', 'eq', 'propertyInCleartext')
      const listObjs3 = await node3.listObjects('properties.prop1', 'eq', 'propertyInCleartext')

      assert.deepStrictEqual(listObjs1.data.length, 2)
      assert.deepStrictEqual(listObjs2.data.length, 2)
      assert.deepStrictEqual(listObjs3.data.length, 2)
    })

    it('query nodes using regular query: properties.prop2 === "qqwwe"', async function () {
      const listObjs1 = await node1.listObjects('properties.prop2', 'eq', 'qqwwe')
      const listObjs2 = await node2.listObjects('properties.prop2', 'eq', 'qqwwe')
      const listObjs3 = await node3.listObjects('properties.prop2', 'eq', 'qqwwe')

      assert.deepStrictEqual(listObjs1.data.length, 1)
      assert(compareObjects(obj1, listObjs1.data[0]))
      assert.deepStrictEqual(listObjs2.data.length, 1)
      assert(compareObjects(obj1, listObjs2.data[0]))
      assert.deepStrictEqual(listObjs3.data.length, 1)
      assert(compareObjects(obj1, listObjs3.data[0]))
    })

    it('query nodes using regular query: properties.prop2 === "123456789"', async function () {
      const listObjs1 = await node1.listObjects('properties.prop2', 'eq', '123456789')
      const listObjs2 = await node2.listObjects('properties.prop2', 'eq', '123456789')
      const listObjs3 = await node3.listObjects('properties.prop2', 'eq', '123456789')

      assert.deepStrictEqual(listObjs1.data.length, 1)
      assert(compareObjects(obj2, listObjs1.data[0]))
      assert.deepStrictEqual(listObjs2.data.length, 1)
      assert(compareObjects(obj2, listObjs2.data[0]))
      assert.deepStrictEqual(listObjs3.data.length, 1)
      assert(compareObjects(obj2, listObjs3.data[0]))
    })

    it('query nodes using regular query: cleartextProperties.prop1 === "propertyInCleartext"', async function () {
      const listObjs1 = await node1.listObjects('cleartextProperties.prop1', 'eq', 'propertyInCleartext')
      const listObjs2 = await node2.listObjects('cleartextProperties.prop1', 'eq', 'propertyInCleartext')
      const listObjs3 = await node3.listObjects('cleartextProperties.prop1', 'eq', 'propertyInCleartext')

      assert.deepStrictEqual(listObjs1.data.length, 2)
      assert.deepStrictEqual(listObjs2.data.length, 2)
      assert.deepStrictEqual(listObjs3.data.length, 2)
    })

    it('query nodes using cleartext query: properties.prop1 === "propertyInCleartext"', async function () {
      const listObjs1 = await node1.listObjectsCleartext('properties.prop1', 'eq', 'propertyInCleartext')
      const listObjs2 = await node2.listObjectsCleartext('properties.prop1', 'eq', 'propertyInCleartext')
      const listObjs3 = await node3.listObjectsCleartext('properties.prop1', 'eq', 'propertyInCleartext')

      assert.deepStrictEqual(listObjs1.data.length, 0)
      assert.deepStrictEqual(listObjs2.data.length, 0)
      assert.deepStrictEqual(listObjs3.data.length, 0)
    })

    it('query nodes using cleartext query: properties.prop2 === "qqwwe"', async function () {
      const listObjs1 = await node1.listObjectsCleartext('properties.prop2', 'eq', 'qqwwe')
      const listObjs2 = await node2.listObjectsCleartext('properties.prop2', 'eq', 'qqwwe')
      const listObjs3 = await node3.listObjectsCleartext('properties.prop2', 'eq', 'qqwwe')

      assert.deepStrictEqual(listObjs1.data.length, 0)
      assert.deepStrictEqual(listObjs2.data.length, 0)
      assert.deepStrictEqual(listObjs3.data.length, 0)
    })

    it('query nodes using cleartext query: properties.prop2 === "123456789"', async function () {
      const listObjs1 = await node1.listObjectsCleartext('properties.prop2', 'eq', '123456789')
      const listObjs2 = await node2.listObjectsCleartext('properties.prop2', 'eq', '123456789')
      const listObjs3 = await node3.listObjectsCleartext('properties.prop2', 'eq', '123456789')

      assert.deepStrictEqual(listObjs1.data.length, 0)
      assert.deepStrictEqual(listObjs2.data.length, 0)
      assert.deepStrictEqual(listObjs3.data.length, 0)
    })

    it('query nodes using cleartext query: cleartextProperties.prop1 === "propertyInCleartext"', async function () {
      const listObjs1 = await node1.listObjectsCleartext('cleartextProperties.prop1', 'eq', 'propertyInCleartext')
      const listObjs2 = await node2.listObjectsCleartext('cleartextProperties.prop1', 'eq', 'propertyInCleartext')
      const listObjs3 = await node3.listObjectsCleartext('cleartextProperties.prop1', 'eq', 'propertyInCleartext')

      assert.deepStrictEqual(listObjs1.data.length, 2)
      assert.deepStrictEqual(listObjs2.data.length, 2)
      assert.deepStrictEqual(listObjs3.data.length, 2)
    })
  })
})
