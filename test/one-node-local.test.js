'use strict'

const assert = require('assert')
const OrbitNode = require('./utils/orbit-node')
const sleep = require('./utils/sleep')
const wait4input = require('./utils/wait-for-input')

const urlDB1 = 'http://localhost:3000'

describe(`OrbitDB - initialization from snapshot`, function () {
  this.timeout(0);

  let o1, dbname

  before(async function () {
    o1 = new OrbitNode(urlDB1)

    let dbs1 = await o1.getDBs()
    assert.equal(dbs1.length, 0)

    dbname = Math.random().toString(16).substring(7)
  })

  after(async function () {
    Object.values(o1.dbs).forEach(async db => await o1.deleteDB(db))
    await sleep(2000)
  })

  describe('One database, 3 nodes', function () {
    let options, db1

    before(async function () {
      console.log(`dbname: ${dbname}`)

      db1 = await o1.createDB(dbname)
      let dbs1 = await o1.getDBs()
      assert.equal(dbs1.length, 1)

      let opLog1 = await db1.getOpLog()
      assert.equal(opLog1.length, 0)

      await wait4input('Press enter when node is ready...')
      dbs1 = await o1.getDBs()
      assert.equal(dbs1.length, 1)
    })

    let pk1

    const compareObjects = (original, retrieved) => {
      if (typeof(original) === 'object') {
        return Object.keys(original).reduce((acc, key) => acc && compareObjects(original[key], retrieved[key]), true)
      } else {
        return original === retrieved
      }
    }

    it('create non encrypted object on node 1 and correctly read it from all nodes', async function () {
      const obj = {
        _id: 11,
        value: "testing node 1",
        details: "cool",
        permissions: { write: [], delete: [] },
        nonEncrypted: true
      }

      await db1.create(obj)

      await wait4input('Press enter when node is ready...')

      let retrieved = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))

      retrieved = await db1.get(obj._id)
      assert.deepStrictEqual(retrieved.length, 1)
      assert(compareObjects(obj, retrieved[0]))
    })
  })
})

