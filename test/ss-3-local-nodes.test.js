const assert = require('assert')
const EunomiaNode = require('./utils/eunomia-node')
const Database = require('./utils/database')
const sleep = require('./utils/sleep')

const sleepAndLog = async (delay, msg) => {
  // console.log(msg)
  await sleep(delay)
}

const urlDB1 = 'http://localhost:3000'
const urlDB2 = 'http://localhost:3001'
const urlDB3 = 'http://localhost:3002'

const urlSS1 = 'http://0.0.0.0:5000'
const urlSS2 = 'http://0.0.0.0:5002'
const urlSS3 = 'http://0.0.0.0:5003'

const compareObjects = (original, retrieved) => {
  if (typeof(original) === 'object') {
    return Object.keys(original).reduce((acc, key) => acc && compareObjects(original[key], retrieved[key]), true)
  } else {
    return original === retrieved
  }
}

describe('Storage Server', function () {
  this.timeout(0)

  let node1, node2, node3

  before(async function () {
    node1 = new EunomiaNode(urlSS1, { dbUrl: urlDB1 })
    await node1.init()
    node2 = new EunomiaNode(urlSS2, { dbUrl: urlDB2, token: node1.token })
    await node2.init()
    node3 = new EunomiaNode(urlSS3, { dbUrl: urlDB3, token: node1.token })
    await node3.init()

    const address = (dbName) => ({ path: dbName })
    node1.db = new Database(urlDB1, address('model'))
    node2.db = new Database(urlDB2, address('model'))
    node3.db = new Database(urlDB3, address('model'))

    node1.id = await node1.getNodeID()
    console.log(`NODE 1 ID: ${node1.id}`)
    node2.id = await node2.getNodeID()
    console.log(`NODE 2 ID: ${node2.id}`)
    node3.id = await node3.getNodeID()
    console.log(`NODE 3 ID: ${node3.id}`)
  })

  it('update public keys', async function () {
    await sleepAndLog(500, 'trigger public key retrieval from blockchain...')
    await node1.db.updatePublicKeys()
    await node2.db.updatePublicKeys()
    await node3.db.updatePublicKeys()
  })

  describe('NO PERMISSIONS + NOT ENCRYPTED -> node 1', async function () {
    let obj, insertedObject

    it('insert object in node 1', async function () {
      await sleepAndLog(500, 'insert object in node 1...')

      obj = {
        properties: {
          prop1: 'sadfsdf',
          prop2: 'qqwwe'
        },
        type: 'oNeTyPe',
      }
      const inserted = await node1.insertObjectNoTemplate(obj)
      insertedObject = inserted.data.object
    })

    it('nodes query DB and read new object', async function () {
      await sleepAndLog(500, 'query servers...')

      const listObjs1 = await node1.listObjects('type', 'eq', 'oNeTyPe')
      const listObjs2 = await node2.listObjects('type', 'eq', 'oNeTyPe')
      const listObjs3 = await node3.listObjects('type', 'eq', 'oNeTyPe')

      assert(listObjs1.data.length >= 1)
      assert(compareObjects(obj, listObjs1.data[0]))
      assert(listObjs2.data.length >= 1)
      assert(compareObjects(obj, listObjs2.data[0]))
      assert(listObjs3.data.length >= 1)
      assert(compareObjects(obj, listObjs3.data[0]))
    })

    let node2updatedObj
    it('node 2 updates object', async function () {
      await sleepAndLog(500, 'insert update in node 2...')

      node2updatedObj = {
        properties: {
          prop1: 'jjjjjj',
          prop2: 'ssssss'
        },
        type: 'nOtHeR+TyPe',
      }
      await node2.updateObjectNoTemplate(insertedObject.id, node2updatedObj)
    })

    it('nodes query DB and read updated object', async function () {
      await sleepAndLog(500, 'query servers...')

      const listObjs1 = await node1.listObjects('type', 'eq', 'nOtHeR+TyPe')
      const listObjs2 = await node2.listObjects('type', 'eq', 'nOtHeR+TyPe')
      const listObjs3 = await node3.listObjects('type', 'eq', 'nOtHeR+TyPe')

      assert(listObjs1.data.length >= 1)
      assert(compareObjects(node2updatedObj, listObjs1.data[0]))
      assert(listObjs2.data.length >= 1)
      assert(compareObjects(node2updatedObj, listObjs2.data[0]))
      assert(listObjs3.data.length >= 1)
      assert(compareObjects(node2updatedObj, listObjs3.data[0]))
    })

    it('node 3 deletes object', async function () {
      await sleepAndLog(500, 'delete object from node 3...')
      await node3.deleteObject(insertedObject.id)
    })

    it('nodes query DB and verify object does not exist anymore', async function () {
      await sleepAndLog(500, 'query servers...')

      await assert.rejects(
        async () => await node1.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })

      await assert.rejects(
        async () => await node2.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })
  })

  describe('PERMISSIONS + NOT ENCRYPTED -> node 1', async function () {
    let obj

    it('insert object in node 1', async function () {
      await sleepAndLog(500, 'PERMISSIONS + NOT ENCRYPTED: insert object in node 1...')

      const permissions = {
        write: [],
        delete: []
      }

      obj = {
        properties: {
          prop1: 'number1',
          prop2: 'number22'
        },
        permissions: permissions,
        type: 'OTHERtype',
      }
      await node1.insertObjectNoTemplate(obj)
    })

    it('nodes query DB and read new object', async function () {
      await sleepAndLog(500, 'query servers...')

      const listObjs1 = await node1.listObjects('type', 'eq', 'OTHERtype')
      assert(listObjs1.data.length >= 1)
      assert(compareObjects(obj, listObjs1.data[0]))

      const listObjs2 = await node2.listObjects('type', 'eq', 'OTHERtype')
      assert(listObjs2.data.length >= 1)
      assert(compareObjects(obj, listObjs2.data[0]))

      const listObjs3 = await node3.listObjects('type', 'eq', 'OTHERtype')
      assert(listObjs3.data.length >= 1)
      assert(compareObjects(obj, listObjs3.data[0]))
    })
  })

  describe('NO PERMISSIONS + ENCRYPTED -> node 1', async function () {
    let obj, insertedObject

    it('insert object in node 1', async function () {
      await sleepAndLog(500, 'insert object in node 1...')

      const permissions = {
        read: ['*']
      }

      obj = {
        properties: {
          prop1: 'numb123er1',
          prop2: 'num2efber22'
        },
        permissions: permissions,
        type: 'ENCRYPTEDdoc',
      }
      const inserted = await node1.insertObjectNoTemplate(obj)
      insertedObject = inserted.data.object
    })

    it('nodes query DB and read new object', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(obj, retObj1))
      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(obj, retObj2))
      const retObj3 = await node3.getObject(insertedObject.id)
      assert(compareObjects(obj, retObj3))
    })
  })

  describe('PERMISSIONS + ENCRYPTED -> node 1', async function () {
    let obj, insertedObject

    it('insert object in node 1', async function () {
      await sleepAndLog(500, 'insert object in node 1...')

      const permissions = {
        read: ['*'],
        write: [],
        delete: []
      }

      obj = {
        properties: {
          prop1: 'ka123',
          prop2: 'boom456'
        },
        permissions: permissions,
        type: 'AllThat',
      }
      const inserted = await node1.insertObjectNoTemplate(obj)
      insertedObject = inserted.data.object
    })

    it('nodes query DB and read new object', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(obj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(obj, retObj2))

      const retObj3 = await node3.getObject(insertedObject.id)
      assert(compareObjects(obj, retObj3))
    })
  })

  describe('changing permissions | nodes 1 (implicitly) and 2 can RWD, node 3 can do nothing', async function () {
    let obj, insertedObject

    it('insert object in node 1', async function () {
      await sleepAndLog(500, 'insert object in node 1...')

      const permissions = {
        read: [ node2.id ],
        write: [ node2.id ],
        delete: [ node2.id ]
      }

      obj = {
        properties: {
          prop1: 'loppo',
          prop2: 'qwert'
        },
        permissions: permissions,
        type: 'huhuhuhuhuh',
      }
      const inserted = await node1.insertObjectNoTemplate(obj)
      insertedObject = inserted.data.object
    })

    it('nodes query DB and read object', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(obj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(obj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    let node2updates
    it('node2 updates object', async function () {
      await sleepAndLog(500, 'insert object update in node 2...')

      node2updates = {
        properties: {
          prop1: 'alsdiu23h',
          prop2: 'ppwqld21'
        },
        type: 'node2updates',
      }
      await node2.updateObjectNoTemplate(insertedObject.id, node2updates)
    })

    it('nodes read updated object', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updates, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updates, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    let node3attemptsUpdate
    it('node3 attempts to update object', async function () {
      await sleepAndLog(500, 'insert object update in node 3...')

      node3attemptsUpdate = {
        properties: {
          prop1: 'adf23edasd',
          prop2: 'asddasd11233324fd'
        },
        type: 'node3attemptsUpdate',
      }
      await assert.rejects( // This fails right away because node3 has no read permissions
        async () => await node3.updateObjectNoTemplate(insertedObject.id, node3attemptsUpdate),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('nodes read previous update (update fails)', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updates, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updates, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    let node1changesPermissions
    it("node1 removes node2's write and delete permissions and allows node3 to RWD", async function () {
      await sleepAndLog(500, 'insert object update in node 1...')

      const permissions = {
        read: [ '*' ],
        write: [ node3.id ],
        delete: [ node3.id ]
      }

      node1changesPermissions = {
        properties: {
          prop1: '8uhgty',
          prop2: '0ak1l23'
        },
        type: 'node1changesPermissions',
        permissions
      }
      await node1.updateObjectNoTemplate(insertedObject.id, node1changesPermissions)
    })

    it('nodes read updated object - node3 can now read', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node1changesPermissions, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node1changesPermissions, retObj2))

      const retObj3 = await node3.getObject(insertedObject.id)
      assert(compareObjects(node1changesPermissions, retObj3))
    })

    let node2attemptsUpdate
    it('node2 attempts to update object', async function () {
      await sleepAndLog(500, 'insert object update in node 2...')

      node2attemptsUpdate = {
        properties: {
          prop1: 'asdkodejdojwodjalsd',
          prop2: 'pod021jdkjsdc2ds'
        },
        type: 'node2attemptsUpdate',
      }
      await node2.updateObjectNoTemplate(insertedObject.id, node2attemptsUpdate)
    })

    it('nodes read previous update (update fails)', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node1changesPermissions, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node1changesPermissions, retObj2))

      const retObj3 = await node3.getObject(insertedObject.id)
      assert(compareObjects(node1changesPermissions, retObj3))
    })

    let node3updates
    it('node3 updates object', async function () {
      await sleepAndLog(500, 'insert object update in node 3...')

      node3updates = {
        properties: {
          prop1: 'lpaslplsapdlpa',
          prop2: '12sa12dx423rf5g5g7'
        },
        type: 'node3updates',
      }
      await node3.updateObjectNoTemplate(insertedObject.id, node3updates)
    })

    it('nodes read previous update (update fails)', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node3updates, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node3updates, retObj2))

      const retObj3 = await node3.getObject(insertedObject.id)
      assert(compareObjects(node3updates, retObj3))
    })

    let node1changesPermissionsAgain
    it("node1 removes node2's read permissions", async function () {
      await sleepAndLog(500, 'insert object update in node 1...')

      const permissions = {
        read: [ node3.id ],
        write: [ node3.id ],
        delete: [ node3.id ]
      }

      node1changesPermissionsAgain = {
        properties: {
          prop1: 'h2grnmsl180',
          prop2: 'pnt4cqa4wlkynck'
        },
        type: 'node1changesPermissionsAgain',
        permissions
      }
      await node1.updateObjectNoTemplate(insertedObject.id, node1changesPermissionsAgain)
    })

    it('nodes read updated object - node2 can no longer read', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node1changesPermissionsAgain, retObj1))

      await assert.rejects(
        async () => await node2.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })

      const retObj3 = await node3.getObject(insertedObject.id)
      assert(compareObjects(node1changesPermissionsAgain, retObj3))
    })

    it('node 3 deletes object', async function () {
      await sleepAndLog(500, 'delete object from node 3...')
      await node3.deleteObject(insertedObject.id)
    })

    it('nodes query DB and verify object does not exist anymore', async function () {
      await sleepAndLog(500, 'query servers...')

      await assert.rejects(
        async () => await node1.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })

      await assert.rejects(
        async () => await node2.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })
  })

  describe('PERMISSIONS + ENCRYPTED -> node 1 | node1:owner; node2:read+write; node3:-', async function () {
    let originalObj
    let insertedObject

    it('insert object in node1', async function () {
      await sleepAndLog(500, 'insert object in node 1...')

      const permissions = {
        read: [ node2.id ],
        write: [ node2.id ],
        delete: []
      }

      originalObj = {
        properties: {
          prop1: 'kab321',
          prop2: 'oom987'
        },
        permissions: permissions,
        type: 'node2CanWrite',
      }

      const inserted = await node1.insertObjectNoTemplate(originalObj)
      insertedObject = inserted.data.object
    })

    it('read in node1 and node2; node3 fails to read', async function () {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(originalObj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(originalObj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    let node2updatedObj
    it('node2 updates', async () => {
      await sleepAndLog(500, 'insert update in node 2...')

      node2updatedObj = {
        properties: {
          prop1: 'boom666',
          prop2: 'khan007'
        },
        type: 'node2Updates',
      }
      await node2.updateObjectNoTemplate(insertedObject.id, node2updatedObj)
    })

    it('node1 and node2 read updated obj; node3 fails', async () => {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node2 attempts delete', async () => {
      await sleepAndLog(500, 'attempt delete in node 2...')
      await node2.deleteObject(insertedObject.id)
    })

    it('node1 and node2 still read updated obj; node3 fails', async () => {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node3 attempts update', async () => {
      await sleepAndLog(500, 'insert update in node 3...')

      const node3updatedObj = {
        properties: {
          prop1: 'node3node3',
          prop2: 'node3node3node3node3'
        },
        type: 'node3Updates!!!',
      }
      await assert.rejects( // This fails right away because node3 has no read permissions
        async () => await node3.updateObjectNoTemplate(insertedObject.id, node3updatedObj),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node1 and node2 still read previously updated obj; node3 still fails to read', async () => {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node2 tries to change permissions via update', async () => {
      await sleepAndLog(500, 'insert permissions-changing update in node 2...')

      const permissions = {
        read: [ node2.id, node3.id ],
        write: [ node2.id, node3.id ],
        delete: [ node2.id, node3.id ]
      }

      node2updatedObj = {
        properties: {
          prop1: 'room237',
          prop2: 'shine12'
        },
        type: 'node2UpdatesAGAIN',
        permissions: permissions
      }
      await node2.updateObjectNoTemplate(insertedObject.id, node2updatedObj)
    })

    it('node1 and node2 read updated obj; node3 still fails', async () => {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node2 attempts delete AGAIN', async () => {
      await sleepAndLog(500, 'attempt delete AGAIN in node 2...')
      await node2.deleteObject(insertedObject.id)
    })

    it('node1 and node2 still read updated obj (NOT DELETED); node3 still fails', async () => {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node3 attempts delete', async () => {
      await sleepAndLog(500, 'attempt delete AGAIN in node 2...')
      await node3.deleteObject(insertedObject.id)
    })

    it('node1 and node2 still read updated obj (NOT DELETED); node3 still fails', async () => {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node3 attempts update and fails', async () => {
      await sleepAndLog(500, 'insert update in node 3...')

      const node3updatedObj = {
        properties: {
          prop1: 'node3node3',
          prop2: 'node3node3node3node3'
        },
        type: 'node3Updates!!!',
      }
      await assert.rejects(
        async () => await node3.updateObjectNoTemplate(insertedObject.id, node3updatedObj),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node1 and node2 still read previously updated obj; node3 still fails to read', async () => {
      await sleepAndLog(500, 'query servers...')

      const retObj1 = await node1.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj1))

      const retObj2 = await node2.getObject(insertedObject.id)
      assert(compareObjects(node2updatedObj, retObj2))

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })

    it('node 1 deletes object', async function () {
      await sleepAndLog(500, 'delete object from node 1...')
      await node1.deleteObject(insertedObject.id)
    })

    it('nodes query DB and verify object does not exist anymore', async function () {
      await sleepAndLog(500, 'query servers...')

      await assert.rejects(
        async () => await node1.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })

      await assert.rejects(
        async () => await node2.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })

      await assert.rejects(
        async () => await node3.getObject(insertedObject.id),
        (err) => {
          assert.deepStrictEqual(err.response.data.status, "NOT_FOUND")
          assert.deepStrictEqual(err.response.data.message, "Couldn't find an object with the id provided")
          return true
        })
    })
  })
})
