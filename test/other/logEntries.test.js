const axios = require('axios')
const EunomiaNode = require('../utils/eunomia-node')
const sleep = require('../utils/sleep')

const PORT1 = process.argv[2] || 'http://146.193.69.132:5000'
const DBPORT1 = process.argv[3] || 'http://146.193.69.132:3000'
const TOKEN1 = process.argv[4]
const PORT2 = process.argv[5] || 'http://146.193.69.133:5000'
const DBPORT2 = process.argv[6] || 'http://146.193.69.133:3000'
const TOKEN2 = process.argv[7]

const main = async () => {
  try {

    const node1 = new EunomiaNode(PORT1, { dbUrl: DBPORT1, token: TOKEN1 })
    await node1.init()

    const node2 = new EunomiaNode(PORT2, { dbUrl: DBPORT2, token: TOKEN2 })
    await node2.init()

    await sleep(500)

    const oplog1 = await node1.getOpLog()
    console.log(`oplog1: ${oplog1.length}`)
    const oplog2 = await node2.getOpLog()
    console.log(`oplog2: ${oplog2.length}`)

    const lcoplog1 = await node1.getOpLog('ledgercache')
    console.log(`lcoplog1: ${lcoplog1.length}`)
    const lcoplog2 = await node2.getOpLog('ledgercache')
    console.log(`lcoplog2: ${lcoplog2.length}`)

  } catch (e) {
    console.log('++++++++++++ THERE WAS AN ERROR ++++++++++++')
    console.log(e)
  }
}

main()

