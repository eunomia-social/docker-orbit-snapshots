const axios = require('axios')
const EunomiaNode = require('../utils/eunomia-node')
const sleep = require('../utils/sleep')

const PORT = process.argv[2] || 'http://146.193.69.132:5000'
const DBPORT = process.argv[3] || 'http://146.193.69.132:3000'
const TOKEN = process.argv[4]

const main = async () => {
  try {
    const node1 = new EunomiaNode(PORT, { dbUrl: DBPORT, token: TOKEN })
    await node1.init()

    await sleep(500)

    const oplog1 = await node1.getOpLog()
    console.log(`oplog1: ${oplog1.length}`)

    const lcoplog1 = await node1.getOpLog('ledgercache')
    console.log(`lcoplog1: ${lcoplog1.length}`)
  } catch (e) {
    console.log('++++++++++++ THERE WAS AN ERROR ++++++++++++')
    console.log(e)
  }
}

main()

