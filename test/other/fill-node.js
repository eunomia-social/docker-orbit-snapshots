const axios = require('axios')
const EunomiaNode = require('../utils/eunomia-node')

// Usage: node fill-node.js [num [url [token]]]

const NUM = process.argv[2] || 500
const URL = process.argv[3] || 'http://localhost:5000'
const TOKEN = process.argv[4]

const main = async () => {
  try {
    const node1 = new EunomiaNode(URL, { token: TOKEN })

    await node1.init()

    console.log("----- node1.insertObject")
    const insertObj = await node1.insertObject({'user': 'glou'})
    console.log(insertObj.data.object)
    const objID = insertObj.data.object.id

    console.log(`----- invoking node1.updateObject ${NUM} times`)
    for (let i = 0; i < NUM; i++) {
      await node1.updateObject(objID, {'user': 'uolg', 'others': `hello${i}`})
    }

    console.log("----- node1.listObjects('user', 'ne', 'jo')")
    let listObjs = await node1.listObjects('user', 'ne', 'jo')
    console.log(listObjs.data.length)

  } catch (e) {
    console.log('++++++++++++ THERE WAS AN ERROR ++++++++++++')
    console.log(e)
  }
}

main()
