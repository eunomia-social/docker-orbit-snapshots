const axios = require('axios')
const EunomiaNode = require('./utils/eunomia-node')

const PORT1 = process.argv[2] || 5000
const DBPORT1 = process.argv[3] || 3000
const TOKEN = process.argv[4]

const main = async () => {
  try {
    
    const node1 = new EunomiaNode(PORT1, { dbPort: DBPORT1, token: TOKEN })

    await node1.init()

    console.log("----- node1.listObjects('user', 'ne', 'jo')")
    let listObjs = await node1.listObjects('user', 'ne', 'jo')
    console.log(listObjs.data.length)

    console.log("----- node1.insertObject({'user': 'asds', 'other': 'ne', 'name': 'jo'})")
    const insertObj = await node1.insertObject({'user': 'asds', 'other': 'ne', 'name': 'jo'})
    console.log(insertObj.data.object)

    console.log("----- node1.listObjects('user', 'ne', 'jo')")
    listObjs = await node1.listObjects('user', 'ne', 'jo')
    console.log(listObjs.data.length)

    console.log('--- node1.getObject(insertObj.data.object.id)')
    const getObj = await node1.getObject(insertObj.data.object.id)
    console.log(getObj)

    console.log("----- node1.updateObject(insertObj.data.object.id, {'user': 'hell', 'other': 'yes', 'name': 'jo', 'cool': 'stuff'})")
    const updateObj = await node1.updateObject(insertObj.data.object.id, {'user': 'hell', 'other': 'yes', 'name': 'jo', 'cool': 'stuff'})
    console.log(updateObj.data.object)

    console.log("----- node1.listObjects('user', 'ne', 'jo')")
    listObjs = await node1.listObjects('user', 'ne', 'jo')
    console.log(listObjs.data.length)

  } catch (e) {
    console.log('++++++++++++ THERE WAS AN ERROR ++++++++++++')
    console.log(e)
  }
}

main()
