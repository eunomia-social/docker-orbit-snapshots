const assert = require('assert')
const OrbitNode = require('./utils/orbit-node')
const sleep = require('./utils/sleep')
const wait4input = require('./utils/wait-for-input')
const { urlDB1, urlDB2 } = require('./utils/config')

const NUMBER_OF_UPDATES = 250

describe(`OrbitDB - initialization from snapshot`, function () {
  this.timeout(0);

  let o1, o2, dbname

  before(async function () {
    o1 = new OrbitNode(urlDB1)
    o2 = new OrbitNode(urlDB2)
  })

  afterEach(async function () {
    await sleep(NUMBER_OF_UPDATES*25)
    Object.values(o1.dbs).forEach(async db => await o1.deleteDB(db))
    Object.values(o2.dbs).forEach(async db => await o2.deleteDB(db))
    await sleep(2000)
  })

  beforeEach(async function () {
    let dbs1 = await o1.getDBs()
    let dbs2 = await o2.getDBs()
    assert.equal(dbs1.length, 0)
    assert.equal(dbs2.length, 0)
    dbname = Math.random().toString(16).substring(7)
  })

  describe('One database', function () {
    let options, db1, db2

    beforeEach(async function () {
      console.log(`dbname: ${dbname}`)

      options = {
        indexSnapshot: true,
        snapshotHeuristics: { type: 'LogSize', limit: 5 }
      }

      db1 = await o1.createDB(dbname, options)
      let dbs1 = await o1.getDBs()
      let dbs2 = await o2.getDBs()
      assert.equal(dbs1.length, 1)
      assert.equal(dbs2.length, 0)

      let opLog1 = await db1.getOpLog()
      assert.equal(opLog1.length, 0)
    })

    async function openSecondDBAndAssert () {
      let opLog1 = await db1.getOpLog()
      assert.equal(opLog1.length, NUMBER_OF_UPDATES)

      let objs = await db1.get('whatAnObject')
      assert.equal(objs.length, 1)
      assert.equal(objs[0]._id, 'whatAnObject')
      assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

      // Wait for node 1 to finish taking snapshots
      // TODO: automate
      await wait4input('Press enter when node 1 is ready...')

      db2 = await o2.openDB(db1.address, options)
      let dbs1 = await o1.getDBs()
      let dbs2 = await o2.getDBs()
      assert.equal(dbs1.length, 1)
      assert.equal(dbs2.length, 1)

      // Wait for second node to initialize index from snapshot
      await new Promise((resolve) => {
        const interval = setInterval(async () => {
          let obj = await db2.get('whatAnObject')
          if (obj &&
              obj[0] &&
              obj[0]._id === 'whatAnObject' &&
              obj[0].value === `yeah${NUMBER_OF_UPDATES-1}`) {

            clearInterval(interval)
            resolve()
          }
        }, 1000)
      })

      console.log('sleeping 3s...')
      await sleep(3000)

      let opLog2 = await db2.getOpLog()
      console.log(`log1: ${opLog1.length}`)
      console.log(`log2: ${opLog2.length}`)
      assert(opLog2.length > 0)
      assert(opLog2.length <= 5)

      let objs2 = await db2.get('whatAnObject')
      assert.equal(objs2.length, 1)
      assert.equal(objs2[0]._id, 'whatAnObject')
      assert.equal(objs2[0].value, `yeah${NUMBER_OF_UPDATES-1}`)
    }

    it('sequential inserts', async function () {
      for (var i = 0; i < NUMBER_OF_UPDATES; i++) {
        await db1.put({_id: 'whatAnObject', value: `yeah${i}`})
      }

      await openSecondDBAndAssert()
    })

    it('parallel inserts', async function () {
      for (var i = 0; i < NUMBER_OF_UPDATES-1; i++) {
        db1.put({_id: 'whatAnObject', value: `yeah${i}`})
      }

      // Wait for all updates to be inserted
      await new Promise((resolve) => {
        const interval = setInterval(async () => {
          const opLog = await db1.getOpLog()
          if (opLog.length === NUMBER_OF_UPDATES-1) {
            clearInterval(interval)
            resolve()
          }
        }, 1000)
      })

      // Insert one last update to make result deterministic
      await db1.put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})

      await openSecondDBAndAssert()
    })
  })

  describe('Two databases', function () {
    let dbname1, dbname2, options

    beforeEach(async function () {
      dbname1 = dbname + '1'
      dbname2 = dbname + '2'
      console.log(`dbnames: ${dbname1} and ${dbname2}`)

      options = {
        indexSnapshot: true,
        snapshotHeuristics: { type: 'LogSize', limit: 5 }
      }

      await o1.createDB(dbname1, options)
      await o1.createDB(dbname2, options)
      let dbs1 = await o1.getDBs()
      let dbs2 = await o2.getDBs()
      assert.equal(dbs1.length, 2)
      assert.equal(dbs2.length, 0)

      let opLog1 = await o1.dbs[dbname1].getOpLog()
      assert.equal(opLog1.length, 0)
      opLog1 = await o1.dbs[dbname2].getOpLog()
      assert.equal(opLog1.length, 0)
    })

    async function openSecondDBAndAssert () {
      // Confirm updates were correctly inserted
      let opLog1 = await o1.dbs[dbname1].getOpLog()
      assert.equal(opLog1.length, NUMBER_OF_UPDATES)
      opLog1 = await o1.dbs[dbname2].getOpLog()
      assert.equal(opLog1.length, NUMBER_OF_UPDATES)

      let objs = await o1.dbs[dbname1].get('whatAnObject')
      assert.equal(objs.length, 1)
      assert.equal(objs[0]._id, 'whatAnObject')
      assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

      objs = await o1.dbs[dbname2].get('whatAnObject')
      assert.equal(objs.length, 1)
      assert.equal(objs[0]._id, 'whatAnObject')
      assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

      // Wait for node 1 to finish taking snapshots
      await wait4input('Press enter when node 1 is ready...')

      // Open databases on second node
      await o2.openDB(o1.dbs[dbname1].address, options)
      await o2.openDB(o1.dbs[dbname2].address, options)
      dbs1 = await o1.getDBs()
      dbs2 = await o2.getDBs()
      assert.equal(dbs1.length, 2)
      assert.equal(dbs2.length, 2)

      // Wait for second node to initialize index from snapshot
      await new Promise((resolve) => {
        const interval = setInterval(async () => {
          let objDB1 = await o2.dbs[dbname1].get('whatAnObject')
          let objDB2 = await o2.dbs[dbname2].get('whatAnObject')
          if (objDB1 && objDB2 &&
              objDB1[0] && objDB2[0] &&
              objDB1[0]._id === 'whatAnObject' && objDB2[0]._id === 'whatAnObject' &&
              objDB1[0].value === `yeah${NUMBER_OF_UPDATES-1}` &&
              objDB2[0].value === `yeah${NUMBER_OF_UPDATES-1}`) {

            clearInterval(interval)
            resolve()
          }
        }, 1000)
      })

      console.log('sleeping 3s...')
      await sleep(3000)

      // Confirm second node correctly applied snapshot
      let opLog2 = await o2.dbs[dbname1].getOpLog()
      console.log(`log2: ${opLog2.length}`)
      assert(opLog2.length > 0)
      assert(opLog2.length <= 5)
      opLog2 = await o2.dbs[dbname2].getOpLog()
      console.log(`log2: ${opLog2.length}`)
      assert(opLog2.length > 0)
      assert(opLog2.length <= 5)

      let objs2 = await o2.dbs[dbname1].get('whatAnObject')
      assert.equal(objs2.length, 1)
      assert.equal(objs2[0]._id, 'whatAnObject')
      assert.equal(objs2[0].value, `yeah${NUMBER_OF_UPDATES-1}`)
      objs2 = await o2.dbs[dbname2].get('whatAnObject')
      assert.equal(objs2.length, 1)
      assert.equal(objs2[0]._id, 'whatAnObject')
      assert.equal(objs2[0].value, `yeah${NUMBER_OF_UPDATES-1}`)
    }

    it('sequential inserts', async function () {
      // Insert updates
      for (var i = 0; i < NUMBER_OF_UPDATES; i++) {
        await o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${i}`})
        await o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${i}`})
      }

      await openSecondDBAndAssert()
    })

    it('parallel inserts', async function () {
      // Insert updates
      for (var i = 0; i < NUMBER_OF_UPDATES-1; i++) {
        o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${i}`})
        o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${i}`})
      }
      // Wait for all updates to be inserted
      await new Promise((resolve) => {
        const interval = setInterval(async () => {
          const opLogDB1 = await o1.dbs[dbname1].getOpLog()
          const opLogDB2 = await o1.dbs[dbname2].getOpLog()
          if (opLogDB1.length === NUMBER_OF_UPDATES-1 && 
              opLogDB2.length === NUMBER_OF_UPDATES-1) {
            clearInterval(interval)
            resolve()
          }
        }, 1000)
      })

      await o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})
      await o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})

      await openSecondDBAndAssert()
    })
  })

  describe('Four databases (but only two contain objects)', function () {
    let dbname1, dbname2, options

    beforeEach(async function () {
      dbname1 = dbname + '1'
      dbname2 = dbname + '2'
      console.log(`dbnames: ${dbname1} and ${dbname2}`)

      options = {
        indexSnapshot: true,
        snapshotHeuristics: { type: 'LogSize', limit: 5 }
      }

      await o1.createDB(dbname1, options)
      await o1.createDB(dbname2, options)
      await o1.createDB(dbname + '3', options)
      await o1.createDB(dbname + '4', options)
      let dbs1 = await o1.getDBs()
      let dbs2 = await o2.getDBs()
      assert.equal(dbs1.length, 4)
      assert.equal(dbs2.length, 0)

      let opLog1 = await o1.dbs[dbname1].getOpLog()
      assert.equal(opLog1.length, 0)
      opLog1 = await o1.dbs[dbname2].getOpLog()
      assert.equal(opLog1.length, 0)
    })

    async function openSecondDBAndAssert () {
      // Confirm updates were correctly inserted
      let opLog1 = await o1.dbs[dbname1].getOpLog()
      assert.equal(opLog1.length, NUMBER_OF_UPDATES)
      opLog1 = await o1.dbs[dbname2].getOpLog()
      assert.equal(opLog1.length, NUMBER_OF_UPDATES)

      let objs = await o1.dbs[dbname1].get('whatAnObject')
      assert.equal(objs.length, 1)
      assert.equal(objs[0]._id, 'whatAnObject')
      assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

      objs = await o1.dbs[dbname2].get('whatAnObject')
      assert.equal(objs.length, 1)
      assert.equal(objs[0]._id, 'whatAnObject')
      assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

      // Wait for node 1 to finish taking snapshots
      await wait4input('Press enter when node 1 is ready...')

      // Open databases on second node
      await o2.openDB(o1.dbs[dbname1].address, options)
      await o2.openDB(o1.dbs[dbname2].address, options)
      await o2.openDB(o1.dbs[dbname + '3'].address, options)
      await o2.openDB(o1.dbs[dbname + '4'].address, options)
      dbs1 = await o1.getDBs()
      dbs2 = await o2.getDBs()
      assert.equal(dbs1.length, 4)
      assert.equal(dbs2.length, 4)

      // Wait for second node to initialize index from snapshot
      await new Promise((resolve) => {
        const interval = setInterval(async () => {
          let objDB1 = await o2.dbs[dbname1].get('whatAnObject')
          let objDB2 = await o2.dbs[dbname2].get('whatAnObject')
          if (objDB1 && objDB2 &&
              objDB1[0] && objDB2[0] &&
              objDB1[0]._id === 'whatAnObject' && objDB2[0]._id === 'whatAnObject' &&
              objDB1[0].value === `yeah${NUMBER_OF_UPDATES-1}` &&
              objDB2[0].value === `yeah${NUMBER_OF_UPDATES-1}`) {

            clearInterval(interval)
            resolve()
          }
        }, 1000)
      })

      console.log('sleeping 3s...')
      await sleep(3000)

      // Confirm second node correctly applied snapshot
      let opLog2 = await o2.dbs[dbname1].getOpLog()
      console.log(`log2: ${opLog2.length}`)
      assert(opLog2.length > 0)
      assert(opLog2.length <= 5)
      
      opLog2 = await o2.dbs[dbname2].getOpLog()
      console.log(`log2: ${opLog2.length}`)
      assert(opLog2.length > 0)
      assert(opLog2.length <= 5)

      let objs2 = await o2.dbs[dbname1].get('whatAnObject')
      assert.equal(objs2.length, 1)
      assert.equal(objs2[0]._id, 'whatAnObject')
      assert.equal(objs2[0].value, `yeah${NUMBER_OF_UPDATES-1}`)
      objs2 = await o2.dbs[dbname2].get('whatAnObject')
      assert.equal(objs2.length, 1)
      assert.equal(objs2[0]._id, 'whatAnObject')
      assert.equal(objs2[0].value, `yeah${NUMBER_OF_UPDATES-1}`)
    }

    it('sequential inserts', async function () {
      // Insert updates
      for (var i = 0; i < NUMBER_OF_UPDATES; i++) {
        await o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${i}`})
        await o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${i}`})
      }

      await openSecondDBAndAssert()
    })

    it('parallel inserts', async function () {
      // Insert updates
      for (var i = 0; i < NUMBER_OF_UPDATES-1; i++) {
        o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${i}`})
        o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${i}`})
      }
      // Wait for all updates to be inserted
      await new Promise((resolve) => {
        const interval = setInterval(async () => {
          const opLogDB1 = await o1.dbs[dbname1].getOpLog()
          const opLogDB2 = await o1.dbs[dbname2].getOpLog()
          if (opLogDB1.length === NUMBER_OF_UPDATES-1 && 
              opLogDB2.length === NUMBER_OF_UPDATES-1) {
            clearInterval(interval)
            resolve()
          }
        }, 1000)
      })

      // Insert one last update to make result deterministic
      await o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})
      await o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})

      await openSecondDBAndAssert()
    })
  })

  // describe('Four databases (but only two contain objects)', function () {
  //   let dbname1, dbname2, options

  //   beforeEach(async function () {
  //     dbname1 = dbname + '1'
  //     dbname2 = dbname + '2'
  //     console.log(`dbnames: ${dbname1} and ${dbname2}`)

  //     options = {
  //       indexSnapshot: true,
  //       snapshotHeuristics: { type: 'LogSize', limit: 5 }
  //     }

  //     await o1.createDB(dbname1, options)
  //     await o1.createDB(dbname2, options)
  //     await o1.createDB(dbname + '3', options)
  //     await o1.createDB(dbname + '4', options)

  //     let opLog1 = await o1.dbs[dbname1].getOpLog()
  //     assert.equal(opLog1.length, 0)
  //     opLog1 = await o1.dbs[dbname2].getOpLog()
  //     assert.equal(opLog1.length, 0)
  //   })

  //   async function openSecondDBAndAssert () {
  //     // Confirm updates were correctly inserted
  //     let opLog1 = await o1.dbs[dbname1].getOpLog()
  //     assert.equal(opLog1.length, NUMBER_OF_UPDATES)
  //     opLog1 = await o1.dbs[dbname2].getOpLog()
  //     assert.equal(opLog1.length, NUMBER_OF_UPDATES)

  //     let objs = await o1.dbs[dbname1].get('whatAnObject')
  //     assert.equal(objs.length, 1)
  //     assert.equal(objs[0]._id, 'whatAnObject')
  //     assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

  //     objs = await o1.dbs[dbname2].get('whatAnObject')
  //     assert.equal(objs.length, 1)
  //     assert.equal(objs[0]._id, 'whatAnObject')
  //     assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

  //     // Wait for node 1 to finish taking snapshots
  //     await wait4input('Press enter when node 1 is ready...')
  //     // console.log('Press enter when node 1 is ready... (auto)')

  //     // Open databases on second node
  //     await o2.openDB(o1.dbs[dbname1].address, options)
  //     await o2.openDB(o1.dbs[dbname2].address, options)
  //     await o2.openDB(o1.dbs[dbname + '3'].address, options)
  //     await o2.openDB(o1.dbs[dbname + '4'].address, options)

  //     // Wait for second node to initialize index from snapshot
  //     await new Promise((resolve) => {
  //       const interval = setInterval(async () => {
  //         let objDB1 = await o2.dbs[dbname1].get('whatAnObject')
  //         let objDB2 = await o2.dbs[dbname2].get('whatAnObject')
  //         if (objDB1 && objDB2 &&
  //             objDB1[0] && objDB2[0] &&
  //             objDB1[0]._id === 'whatAnObject' && objDB2[0]._id === 'whatAnObject' &&
  //             objDB1[0].value === `yeah${NUMBER_OF_UPDATES-1}` &&
  //             objDB2[0].value === `yeah${NUMBER_OF_UPDATES-1}`) {

  //           clearInterval(interval)
  //           resolve()
  //         }
  //       }, 1000)
  //     })

  //     // console.log('sleeping 5s...')
  //     // await sleep(5000)
  //     // Wait for node 1 to finish taking snapshots
  //     await wait4input('Press enter when node 2 is ready...')

  //     // Confirm second node correctly applied snapshot
  //     let opLog2 = await o2.dbs[dbname1].getOpLog()
  //     console.log(`log2: ${opLog2.length}`)

  //     opLog2 = await o2.dbs[dbname2].getOpLog()
  //     console.log(`log2: ${opLog2.length}`)

  //     let objs2 = await o2.dbs[dbname1].get('whatAnObject')
  //     console.log(`obj db1: ${objs2}`)
  //     objs2 = await o2.dbs[dbname2].get('whatAnObject')
  //     console.log(`obj db2: ${objs2}`)
  //   }

  //   it('sequential inserts', async function () {
  //     // Insert updates
  //     for (var i = 0; i < NUMBER_OF_UPDATES; i++) {
  //       await o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${i}`})
  //       await o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${i}`})
  //     }

  //     await openSecondDBAndAssert()
  //   })

  //   // it('parallel inserts', async function () {
  //   //   // Insert updates
  //   //   for (var i = 0; i < NUMBER_OF_UPDATES-1; i++) {
  //   //     o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${i}`})
  //   //     o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${i}`})
  //   //   }
  //   //   // Wait for all updates to be inserted
  //   //   await new Promise((resolve) => {
  //   //     const interval = setInterval(async () => {
  //   //       const opLogDB1 = await o1.dbs[dbname1].getOpLog()
  //   //       const opLogDB2 = await o1.dbs[dbname2].getOpLog()
  //   //       if (opLogDB1.length === NUMBER_OF_UPDATES-1 && 
  //   //           opLogDB2.length === NUMBER_OF_UPDATES-1) {
  //   //         clearInterval(interval)
  //   //         resolve()
  //   //       }
  //   //     }, 1000)
  //   //   })

  //   //   // Insert one last update to make result deterministic
  //   //   await o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})
  //   //   await o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})

  //   //   await openSecondDBAndAssert()
  //   // })
  // })

  // describe('Four databases (but only two contain objects)', function () {
  //   let dbname1, dbname2, options

  //   beforeEach(async function () {
  //     dbname1 = dbname + '1'
  //     dbname2 = dbname + '2'
  //     console.log(`dbnames: ${dbname1} and ${dbname2}`)

  //     options = {
  //       indexSnapshot: true,
  //       snapshotHeuristics: { type: 'LogSize', limit: 5 }
  //     }

  //     await o1.createDB(dbname1, options)
  //     await o1.createDB(dbname2, options)
  //     await o1.createDB(dbname + '3', options)
  //     await o1.createDB(dbname + '4', options)

  //     let opLog1 = await o1.dbs[dbname1].getOpLog()
  //     assert.equal(opLog1.length, 0)
  //     opLog1 = await o1.dbs[dbname2].getOpLog()
  //     assert.equal(opLog1.length, 0)
  //   })

  //   async function openSecondDBAndAssert () {
  //     // Confirm updates were correctly inserted
  //     let opLog1 = await o1.dbs[dbname1].getOpLog()
  //     assert.equal(opLog1.length, NUMBER_OF_UPDATES)
  //     opLog1 = await o1.dbs[dbname2].getOpLog()
  //     assert.equal(opLog1.length, NUMBER_OF_UPDATES*2)

  //     let objs = await o1.dbs[dbname1].get('whatAnObject')
  //     assert.equal(objs.length, 1)
  //     assert.equal(objs[0]._id, 'whatAnObject')
  //     assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

  //     objs = await o1.dbs[dbname2].get('whatAnObject')
  //     assert.equal(objs.length, 0)
  //     // assert.equal(objs[0]._id, 'whatAnObject')
  //     // assert.equal(objs[0].value, `yeah${NUMBER_OF_UPDATES-1}`)

  //     // Wait for node 1 to finish taking snapshots
  //     await wait4input('Press enter when node 1 is ready...')
  //     // console.log('Press enter when node 1 is ready... (auto)')

  //     // Open databases on second node
  //     await o2.openDB(o1.dbs[dbname1].address, options)
  //     await o2.openDB(o1.dbs[dbname2].address, options)
  //     await o2.openDB(o1.dbs[dbname + '3'].address, options)
  //     await o2.openDB(o1.dbs[dbname + '4'].address, options)

  //     // Wait for second node to initialize index from snapshot
  //     await new Promise((resolve) => {
  //       const interval = setInterval(async () => {
  //         let objDB1 = await o2.dbs[dbname1].get('whatAnObject')
  //         if (objDB1 &&
  //             objDB1[0] &&
  //             objDB1[0]._id === 'whatAnObject' &&
  //             objDB1[0].value === `yeah${NUMBER_OF_UPDATES-1}`) {

  //           clearInterval(interval)
  //           resolve()
  //         }
  //       }, 1000)
  //     })

  //     // console.log('sleeping 5s...')
  //     // await sleep(5000)
  //     // Wait for node 1 to finish taking snapshots
  //     await wait4input('Press enter when node 2 is ready...')

  //     // Confirm second node correctly applied snapshot
  //     let opLog2 = await o2.dbs[dbname1].getOpLog()
  //     console.log(`log2: ${opLog2.length}`)

  //     opLog2 = await o2.dbs[dbname2].getOpLog()
  //     console.log(`log2: ${opLog2.length}`)

  //     let objs2 = await o2.dbs[dbname1].get('whatAnObject')
  //     console.log(`obj db1: ${objs2}`)
  //     objs2 = await o2.dbs[dbname2].get('whatAnObject')
  //     console.log(`obj db2: ${objs2}`)
  //   }

  //   it('sequential inserts (and deletes on 2nd DB)', async function () {
  //     // Insert updates
  //     for (var i = 0; i < NUMBER_OF_UPDATES; i++) {
  //       await o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${i}`})
  //       await o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${i}`})
  //       await o1.dbs[dbname2].delete('whatAnObject')
  //     }

  //     await openSecondDBAndAssert()
  //   })

  //   // it('parallel inserts', async function () {
  //   //   // Insert updates
  //   //   for (var i = 0; i < NUMBER_OF_UPDATES-1; i++) {
  //   //     o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${i}`})
  //   //     o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${i}`})
  //   //   }
  //   //   // Wait for all updates to be inserted
  //   //   await new Promise((resolve) => {
  //   //     const interval = setInterval(async () => {
  //   //       const opLogDB1 = await o1.dbs[dbname1].getOpLog()
  //   //       const opLogDB2 = await o1.dbs[dbname2].getOpLog()
  //   //       if (opLogDB1.length === NUMBER_OF_UPDATES-1 && 
  //   //           opLogDB2.length === NUMBER_OF_UPDATES-1) {
  //   //         clearInterval(interval)
  //   //         resolve()
  //   //       }
  //   //     }, 1000)
  //   //   })

  //   //   // Insert one last update to make result deterministic
  //   //   await o1.dbs[dbname1].put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})
  //   //   await o1.dbs[dbname2].put({_id: 'whatAnObject', value: `yeah${NUMBER_OF_UPDATES-1}`})

  //   //   await openSecondDBAndAssert()
  //   // })
  // })
})

