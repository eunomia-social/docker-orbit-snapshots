# EUNOMIA OrbitDB Docker
With snapshots enabled + access control enforcement system

## orbitdb options

Set as `environment` variables in the _docker-compose_. See below for an example.

- `BLOCKCHAIN_URL`: the address where the blockchain can be accessed
- `SECRET_SHARING_THRESHOLD`: the threshold number of shares needed to reconstructed a key
- `ACE_NODE_ID`: the node's ID which is used to register it in the blockchian
- `KEY_UPDATE_PERIOD`: periodic time interval in milliseconds at which the blockchain is queried for public keys - every _x_ milliseconds the keys are updated
- `KEY_RETRIEVAL_TIMEOUT`: timeout in milliseconds after which a key retrieval should abort - key retrieval aborts after _x_ milliseconds
- `MAX_PUBSUB_MSG_BYTES`: maximum number of bytes in a singlepart pubsub message - messages over this size are split and sent in parts


## Installation
Clone repository by running
```sh
git clone -b develop --recurse-submodules https://git-ext.inov.pt/cyber/eunomia/docker-orbit-snapshots.git
```

## Generate Docker image
Run
```sh
docker build -t orbit-snapshots .
```

## Example docker-compose
```sh
version: '3'

services:
    ipfs:
        ...

    orbitdb:
        container_name: orbitdb
        image: orbit-snapshots:latest
        environment:
            IPFS_HOST: 'ipfs'
            ORBITDB_DIR: '/orbitdb'
            FORCE_HTTP1: 'true'
            ANNOUNCE_DBS: 'true'
            LOG: 'DEBUG'
            BLOCKCHAIN_URL: 'http://mockbl:8973' # blockchain's address
            SECRET_SHARING_THRESHOLD: 2 # shares needed to reconstructed a key
            ACE_NODE_ID: 'ec5792be-7fe5-11ea-84a4-0bc5ec2bb860' # should be the same in storage-server params
            KEY_UPDATE_PERIOD: 900000 # public keys updated every 15 minutes
            KEY_RETRIEVAL_TIMEOUT: 30000 # key retrieval aborts after 30 seconds
            MAX_PUBSUB_MSG_BYTES: 500000 # pubsub messages split if larger than ~500KB
        depends_on:
            - ipfs
            # - ipfs-cluster
        ports:
            - "3000:3000"
        volumes:
            - ./orbitdb
        networks:
            esn_net:
                ipv4_address: 172.23.0.4
```

# Notes
For adding submodules:
```sh
git submodule add https://git-ext.inov.pt/cyber/eunomia/orbit-db.git
git submodule add https://git-ext.inov.pt/cyber/eunomia/orbit-db-store.git
git submodule add https://git-ext.inov.pt/cyber/eunomia/orbit-db-docstore.git
git submodule add https://git-ext.inov.pt/cyber/eunomia/eunomia-access-control.git
```

For changing submodule branch:
```sh
git config -f .gitmodules submodule.orbit-db.branch develop
git config -f .gitmodules submodule.orbit-db-store.branch develop
git config -f .gitmodules submodule.orbit-db-docstore.branch develop
git config -f .gitmodules submodule.eunomia-access-control.branch develop
```
